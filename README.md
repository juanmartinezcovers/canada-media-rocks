## Installing / Getting started

### 1. Clone the repository

-HTTP
```https://gitlab.com/sigma-studios/canada-rocks-media/website.git```

-SSH
```git@gitlab.com:sigma-studios/canada-rocks-media/website.gitt```

Checkout to develop branch:
```$ git checkout develop```

You should now have the following structure:

```
canada
└───scripts
│   └───...
└───src
│   └───themes
│   └───plugins
│   └───gulp
│   .env
│   docker-compose.yml
│   Dockerfile
│   ...
```

The work directory will be **src**. Themes folder correspond to the themes on wordpress and Plugins folder to the plugins installes on this wordpress.

## Development


### Pre-requisites
You must have [Docker](https://www.docker.com/) installed on your machine. Everything outlined in this project gets run within the Docker container. Docker will download them and install them into the container during the build process.

### Setup

#### Develop Domain (Optional)

You may want to add a `host` entry to your machine for the website you'll be creating. 

Run `$ sudo nano /etc/hosts` 
Then  append the following line `127.0.0.1	dev.canada.com` to the hosts file. 

This will ensure, whenever you hit `dev.canada.com` on your machine, it points to your machine than going out to the internet.

#### Setting the environment

Run `docker-compose build`

#### Run the project

Run `docker-compose up` or `docker-compose up -d`

#### Paths for Gulp 
Gulp is configured to watch for any JS and SCSS files within your themes. JS is split into two categories, one being the plugins, and the second being the app.js. We separated the plugins so they can be loaded first to the page. Then the app.js can utilize that.

The files must be placed like the following:
```
- themes
│   └───my-theme
    │   └───src
        │   └───sass
            │   └───styles.scss
            │   └───...
        │   └───js
            │   └───script1.js
            │   └───script2.js
                │   └───plugins
                    │   └───jquery.js
                    │   └───flexslider.js
    │   └───...
```

#### Managing Database
The project comes with the Adminer. You can access the Adminer at [localhost:8080](http://localhost:8080).

### Deploy

For a live deploy go to `.env` file and change the `SITE_URL` for the domain of wordpress. 
Build the containers and then run them.

`$ docker-compose build`

<<<<<<< HEAD
`$ docker-compose up -d`
=======
`$ docker-compose up -d`
>>>>>>> aff1b8bd3f10c14df45a5e5ec75b72ab661f698c
