<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package BuddyBoss_Theme
 */

get_header();
?>

<?php get_template_part( 'template-parts/share' ); ?>
<div id="primary" class="content-area">
		<main id="main" class="site-main d-flex flex-column">
      <?php 
     
      $id = get_the_id();
      $event = get_post_meta( $id, 'ev_id', true );
      $stream = do_shortcode('[streaming id=' . $id . ' ]');
      echo do_shortcode('[tribe_tickets_protected_content post_id=' . $event . ']' . $stream . '[/tribe_tickets_protected_content]');
      $tickets = do_shortcode('[tribe_tickets post_id="' . $event . '"]');
      $info = '<p>You dont have any tickets for this show, please buy one to get access to the streaming</p>';
	   $countdown = do_shortcode('[tribe_event_countdown id="' . $event . '" show_seconds="yes"]');	
      ?>	
			
			<div class="countdown d-flex flex-column align-items-center mb-5 order-1">
				<h3>
					This event starts in:
				</h3>
				<?php echo $countdown; ?>
			</div>
      
         <div class="get-tickets d-flex flex-column align-items-center mb-5 order-2">
            <?php  echo do_shortcode('[tribe_tickets_protected_content post_id="' . $event .  '" ticketed="0"]' . $info . $tickets . '[/tribe_tickets_protected_content]'); ?>
         </div>
			
			<div class="mt-5 upcoming-events order-3">
				<?php echo do_shortcode('[tribe_events view="photo" category="online-events" tribe-bar="false"]'); ?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
<?php
    if ( ! is_user_logged_in() ) {
       $args = array(
          'echo'           => true,
          'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
          'form_id'        => 'loginform',
          'label_username' => __( 'Username or Email Address' ),
          'label_password' => __( 'Password' ),
          'label_remember' => __( 'Remember Me' ),
          'label_log_in'   => __( 'Log In' ),
          'id_username'    => 'user_login',
          'id_password'    => 'user_pass',
          'id_remember'    => 'rememberme',
          'id_submit'      => 'wp-submit',
          'remember'       => true,
       );
       ob_start();
       ?>
       <!-- Modal -->
       <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog custom-modal">
         <?php 
            $streamPost = get_post( $id );
            $slug = $streamPost->post_name; 
            $url = get_home_url() . '/event/' . $slug;
         ?>
            <p>If you dont have a ticket for this event, go buy one <a href="<?php echo $url ?>">here</a></p>
         </div>
          <div class="modal-dialog">
             <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Please login to verify you</h5>
                </div>
                <div class="modal-body">
                   <div class="login-form-stream col-md-8 mx-auto">
                      <?php wp_login_form( $args ); ?>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <script>
          jQuery(document).ready(function($){
             $('#exampleModal').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
             });

             $('.login-username').addClass('d-flex flex-column');
             $('.login-password').addClass('d-flex flex-column');
             $('.login-submit').css('float', 'right');
             $('.site').css('filter','blur(5px)');
          });
       </script>
       <?php
       $login = ob_get_contents();
       ob_end_clean();
       echo $login;
    }
