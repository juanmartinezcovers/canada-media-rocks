<?php
/*
Plugin Name: Streaming Add-On for Events Calendar
Plugin URI: http://sigmastudio.la
Description: Streaming add on for events calendar with shortcodes for embed the streaming
Version: 1.3.0
Author: Juan Martínez Sigma Studios
License: GPLv2 or later
*/

/**
* @package StreamAddOn
*/

//Checking if wordpress is initiated 
if( ! defined('ABSPATH') ) {
  die;
}

class streamAddOn
{
  function __construct() {
    add_action( 'init', array( $this, 'stream_post_type' ) );
    add_action( 'add_meta_boxes', array( $this , 'bp_register_meta_boxes' ) );
    add_action( 'save_post_stream', array( $this, 'bp_save_meta_box' ) );
    add_action( 'manage_stream_posts_columns' , array( $this, 'streaming_column_shortcode' ) );
    add_action('manage_stream_posts_custom_column',array( $this, 'streaming_shortcode_handler' ), 10, 2);
    add_filter('single_template', array( $this, 'my_custom_template' ));
  }

  function register() {
    add_action('wp_enqueue_scripts', array( $this, 'enqueue' ));
  }

  function enqueue() {
    wp_enqueue_style('modal', plugins_url('/assets/modal.css', __FILE__));
    wp_enqueue_script( 'plugin-streaming-js', plugins_url('assets/plugin-streaming.js', __FILE__), array('jquery'));
	  if("stream" === get_post_type()  ){
		  wp_enqueue_style('grid-css', plugins_url('/assets/grid.min.css', __FILE__));
	  }
  }

  function addStream() {
    add_action('publish_tribe_events', array( $this , 'bp_create_streaming_for_event' ));
  }

  function deleteStream() {
    add_action('trash_tribe_events', 'bp_delete_streaming_for_event' );
  }

  function activate() {
    // Flush Rewrite Rules
    flush_rewrite_rules();
    // generated CPT
    $this->stream_post_type();
  }

  function deactivate() {
    // Flush Rewrite Rules
    flush_rewrite_rules();
  }

  function stream_post_type() {
    /* Labels that would appear on admin */
	$labels = array(
		'name'               => _x( 'Streams', 'post type general name', 'text-domain' ),
		'singular_name'      => _x( 'Stream', 'post type singular name', 'text-domain' ),
		'menu_name'          => _x( 'Streams', 'admin menu', 'text-domain' ),
		'add_new'            => _x( 'Add New', 'stream', 'text-domain' ),
		'add_new_item'       => __( 'Add new stream', 'text-domain' ),
		'new_item'           => __( 'New Stream', 'text-domain' ),
		'edit_item'          => __( 'Edit Stream', 'text-domain' ),
		'view_item'          => __( 'View Stream', 'text-domain' ),
		'all_items'          => __( 'All streams', 'text-domain' ),
		'search_items'       => __( 'Search streams', 'text-domain' ),
		'not_found'          => __( 'Streams not found', 'text-domain' ),
		'not_found_in_trash' => __( 'Nothing here', 'text-domain' )
	);

    /* Custom post type args */
	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description', 'text-domain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'stream' ),
		'capability_type'    => 'post',
    'has_archive'        => true,
    'menu_icon'          => 'dashicons-video-alt',
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields' )
	);

	register_post_type( 'stream', $args );
  }

  function bp_register_meta_boxes() {
    add_meta_box( 'streaming-source-wrapper', 'Streaming Source', array( $this, 'bp_metabox_display_streaming' ) , 'stream', 'side', 'high' );
  }


  function bp_metabox_display_streaming( $post ) {
    // metabox display callback
    $ev_id = get_post_meta( $post->ID, 'ev_id', true );
    $streaming = get_post_meta( $post->ID, 'streaming', true );
    
    // Nonce for the custom field, would use it later on bp_save_meta_box
    wp_nonce_field( 'bp_meta_box_nonce', 'meta_box_nonce' );
    
    
    echo '<p><label for="streaming_label">Streaming Source</label> <input type="text" name="streaming" id="streaming" value="'. $streaming .'" /></p>';
  }

  
  function bp_save_meta_box( $post_id ) {

    // saving data to db

    // Check if is autosaving
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // Check the nonce on the callback
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'bp_meta_box_nonce' ) ) return;
    // Check if user can edit post
    if( !current_user_can( 'edit_post' ) ) return;
    
    
    // Saving the data on database
    if( isset( $_POST['streaming'] ) ) 
    update_post_meta( $post_id, 'streaming', $_POST['streaming'] );
    if( isset( $_POST['ev_id'] ) ) 
    update_post_meta( $post_id, 'ev_id', $_POST['ev_id'] );

  }

  function bp_delete_streaming_for_event( $post_id ) {

    global $post;

    $post_id = get_the_id();

    $args = array(
      'post_type' => 'stream',
      'meta_key' => 'ev_id',
      'meta_input' => $post_id
    );

    $query = WP_Query( $args );

    if( $query->have_posts() ) {
      $streamID = $query->get_queried_object_id();
      wp_delete_post( $streamID, false );
    }
  }

  function bp_create_streaming_for_event( $post_id ){

    global $post;
    global $wpdb;

    $post_id = get_the_id();
    $title = $_POST['post_title'];
    $post_link = get_permalink( $post_id );
    $uri_path = parse_url($post_link, PHP_URL_PATH);
    $slug = explode('/', $uri_path);
    $stream = $_POST['_ecp_custom_8'];

    $content = '';
  
    $meta = array(
      'streaming' => $stream,
      'ev_id' => $post_id
    );
  
    $stream = array(
      'post_type' => 'stream',
      'post_title' => $title,
      'post_name' => $slug[2],
      'post_content' => $content,
      'post_status' => 'publish',
      'meta_input' => $meta
    );
  
  
    //Query the stream that has relationship with event
    $args = array(
      'post_type' => 'stream',
      'meta_key' => 'ev_id',
      'meta_value' => $post_id
    );
  
    $query = new WP_query( $args );
    //checking the streams already created to avoid duplication
    if( !($query->have_posts()) ){
      //Checking if post is assigned to an online event
      if( has_term('online-events', 'tribe_events_cat', $post_id) ) {
        wp_insert_post( $stream );
      }
    }    
  }

  function streaming_column_shortcode( $defaults ){
    $defaults['shortcode']  = 'Shortcode';
    return $defaults;
  }

  function streaming_shortcode_handler( $column_name, $post_ID ){
    if( $column_name === 'shortcode' ){
      echo '[streaming id=' . $post_ID .']';
    }
  }

  function activate_shortcode() {
    add_shortcode('streaming', array( $this, 'streaming_shortcode' ));
  }

  function streaming_shortcode( $atts ) {
  
    $atts = shortcode_atts(
        array(
          'id' => null
        ), $atts, 'streaming' );
  
    $args = array(
      'post_type' => 'streaming',
      'p' => $atts['id']
    );
    
    $url = get_post_meta($atts['id'], 'streaming', true);
    $youtube = 'www.youtube.com';
    $facebook = "www.facebook.com";
    $twitch = 'www.twitch.tv';
    $vimeo = 'vimeo.com';
    
    $url = filter_var( $url, FILTER_SANITIZE_URL);
  
   if ( empty( $url ) ){

    // The method which finds the video ID
    function getLiveVideoID($channelId)
    {
        $videoId = null;
    
        // Fetch the livestream page
        if($data = file_get_contents('https://www.youtube.com/embed/live_stream?channel='.$channelId))
        {
            // Find the video ID in there
            if(preg_match('/\'VIDEO_ID\': \"(.*?)\"/', $data, $matches))
                $videoId = $matches[1];
            else
                throw new Exception('Couldn\'t find video ID');
        }
        else
            throw new Exception('Couldn\'t fetch data');
    
        return $videoId;
    }

    try {
        $YTchannel = 'UCCCR7vfv1r1ey_K7v6EQpFg';
        // $YTchannel = 'UCsDj8VuZZoO8aEkioY88IxA';
        $videoId = getLiveVideoID( $YTchannel );
        ob_start();
        ?>
        <div class="row order-2" style="width: 100% !important;">
          <div class="col-md-8">
            <iframe width="100%" height="500px" src="https://www.youtube.com/embed/live_stream?channel=<?php echo $YTchannel; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <div class="col-md-4">
            <iframe width="100%" height="100%" src="https://www.youtube.com/live_chat?v=<?php echo $videoId; ?>&amp;embed_domain=<?php echo $_SERVER['SERVER_NAME'] ?>" frameborder="0"></iframe>
          </div>
        </div>
        <?php
        $youtubeEmbed = ob_get_contents();
        ob_end_clean();
        //Cleaning content and returning
        return $youtubeEmbed;
       
    } catch(Exception $e) {
        // Echo the generated error
        $error = $e->getMessage();
        echo 'Error: ' . $error;
    }
   } elseif ( filter_var($url, FILTER_VALIDATE_URL) !== false ){
      
      //Getting the domain host
      $social = parse_url( $url, PHP_URL_HOST );
      
      if ( $social === $youtube ) {
        //Parsing url to get the id of the youtube video
        parse_str( parse_url( $url, PHP_URL_QUERY ), $id );
        //Rendering the iframe for youtube
        ob_start();
        ?>
        <div class="row order-2" style="width: 100% !important">
          <div class="col-md-8">
            <iframe width="100%" height="500px" src="https://www.youtube.com/embed/<?php echo $id['v'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <div class="col-md-4">
            <iframe width="100%" height="100%" src="https://www.youtube.com/live_chat?v=<?php echo $id['v']?>&amp;embed_domain=<?php echo $_SERVER['SERVER_NAME'] ?>" frameborder="0"></iframe>
          </div>
        </div>
        <?php
        $youtubeEmbed = ob_get_contents();
        ob_end_clean();
        //Cleaning content and returning
        return $youtubeEmbed;

      } elseif ( $social === $facebook) {

        ob_start();
        ?>
        <!-- Loading facebook SDK-->
        <div id="fb-root"></div>
        <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>
        <!-- Facebook embed -->
        <div class="row">
        <div class="col-md-12 mx-auto">
        <div class="fb-video"
        data-href="<?php echo $url ?>"
        data-width="700"
        data-allowfullscreen="true"></div>
        </div>
        </div>
        <?php
        $facebookEmbed = ob_get_contents();
        ob_end_clean();
        return $facebookEmbed;

      } elseif( $social === $twitch ) {

        //Getting the channel of twitch
        $channel = ltrim( parse_url( $url, PHP_URL_PATH), '/' );
        ob_start();
        ?>
          <!-- Add a placeholder for the Twitch embed -->
          <div id="twitch-embed"></div>

          <!-- Load the Twitch embed script -->
          <script src="https://embed.twitch.tv/embed/v1.js"></script>

          <!-- Create a Twitch.Embed object that will render within the "twitch-embed" root element. -->
          <script type="text/javascript">
            new Twitch.Embed("twitch-embed", {
              width: 854,
              height: 480,
              channel: "<?php echo $channel ?>",
              // only needed if your site is also embedded on embed.example.com and othersite.example.com 
              parent: ["<?php echo $_SERVER['SERVER_NAME']?>"]
            });
          </script>
        <?php
        $twitchEmbed = ob_get_contents();
        ob_end_clean();
        return $twitchEmbed;

      } elseif ( $social === $vimeo ) {
        $id = ltrim( parse_url( $url, PHP_URL_PATH ), '/' );
        ob_start();
        ?>
        <!-- Vimeo embed container-->
        <iframe src="https://player.vimeo.com/video/<?php echo $id ?>?color=eb007c" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        <?php
        $vimeoStream = ob_get_contents();
        ob_end_clean();
        return $vimeoStream;
      }
    } 
  }

  function my_custom_template( $single ) {

    global $post;

    /* Checks for single template by post type */
    if ( $post->post_type == 'stream' ) {
        if ( file_exists( plugin_dir_path( __FILE__ ) . '/template/single-stream.php' ) ) {
            return plugin_dir_path( __FILE__ ) . '/template/single-stream.php';
        }
    }

    return $single;

  }

}

if ( class_exists( 'streamAddOn' ) ) {
  $streamAddOn = new streamAddOn();
  $streamAddOn->addStream();
  $streamAddOn->activate_shortcode();
  $streamAddOn->deleteStream();
  $streamAddOn->register();
}

// Activation
register_activation_hook( __FILE__, array( $streamAddOn, 'activate' ) );
// Deactivation
register_deactivation_hook( __FILE__, array( $streamAddOn, 'deactivate' ) );