<?php

/**
 * Trigger this file on plugin Uninstall
 * @package StreamAddOn
 */

 if ( ! defined('WP_UNISTALL_PLUGIN') ) {
   die;
 }

 // Clear database stored data
global $wpdb;
$wpdb->query( "DELETE FROM wp_posts WHERE post_type = 'stream'" );
$wpdb->query( "DELETE FROM wp_postmeta WHERE post_id NOT IN ( SELECT id FROM wp_posts )" );