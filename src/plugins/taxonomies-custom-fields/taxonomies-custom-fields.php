<?php 

/*
Plugin Name: TCF - Taxonomies Custom Fields
Plugin URI: http://sigmastudio.la
Description: Add custom fields for taxonomies
Version: 1.0.0
Author: Juan Martínez at Sigma Studios
License: GPLv2 or later
*/

/**
* @package TCF
*/

//Checking if wordpress is initiated 
if( ! defined('ABSPATH') ) {
    die;
}

//include the main class file
require_once("Tax-meta-class/Tax-meta-class.php");
if (is_admin()){
  /* 
   * prefix of meta keys, optional
   */
  $prefix = 'bb_';
  /* 
   * configure your meta box
   */
  $config = array(
    'id' => 'featured-taxonomy-img',          // meta box id, unique per meta box
    'title' => 'Featured Image',          // meta box title
    'pages' => array('category'),        // taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),            // list of meta fields (can be added by field arrays)
    'local_images' => false,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => false          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );
  
  
  /*
   * Initiate your meta box
   */
  $my_meta =  new Tax_Meta_Class($config);
  
  /*
   * Add fields to your meta box
   */
  
  //file upload field
  $my_meta->addFile($prefix.'file_field_id',array('name'=> __('My File ','tax-meta')));

  //Finish Meta Box Decleration
  $my_meta->Finish();
}
