<?php

/**
 * adds custom tab link on profile page
 */

function bp_custom_tab_videos() {

  global $bp;

  $args = array(
          'name' => __('Videos', 'buddypress'),
          'slug' => 'videos',
          'default_subnav_slug' => 'videos',
          'position' => 52,
          'screen_function' => 'videos_screen',
          'item_css_id' => 'videos'
  );

  bp_core_new_nav_item( $args );
}
add_action( 'bp_setup_nav', 'bp_custom_tab_videos', 99 );

function bp_custom_tab_music_library() {

  global $bp;

  $args = array(
          'name' => __('Music Library', 'buddypress'),
          'slug' => 'music-library',
          'default_subnav_slug' => 'music-library',
          'position' => 53,
          'screen_function' => 'music_library_screen',
          'item_css_id' => 'music-library'
  );

  bp_core_new_nav_item( $args );
}
add_action( 'bp_setup_nav', 'bp_custom_tab_music_library', 99 );

function bp_custom_tab_shows() {

  global $bp;

  $userID = bbp_get_user_id();

  $args = array(
          'name' => __('Shows', 'buddypress'),
          'slug' => 'shows',
          'default_subnav_slug' => 'shows',
          'position' => 50,
          'screen_function' => 'shows_screen',
          'item_css_id' => 'shows'
  );

  $memberType = bp_get_member_type( $userID, true );

  if ( $memberType === 'musician' or $memberType === 'Musician' ) {
    bp_core_new_nav_item( $args );
  }
 
}
add_action( 'bp_setup_nav', 'bp_custom_tab_shows', 99 );

/**
* the calback function from our nav item arguments
*/

function videos_screen() {
  add_action( 'bp_template_content', 'bp_custom_screen_videos' );
  bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function music_library_screen() {
  add_action( 'bp_template_content', 'bp_custom_screen_music_library' );
  bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function shows_screen() {
  add_action( 'bp_template_content', 'bp_custom_screen_shows' );
  bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

/**
* the function hooked to bp_template_content, this hook is in plugins.php
*/

function bp_custom_screen_videos() {

  get_template_part('templates/bp', 'videos');
 
}

function bp_custom_screen_music_library() {

  get_template_part('templates/bp', 'music-library');
 
}


function bp_custom_screen_shows() {

  get_template_part('templates/bp', 'shows');
 
}