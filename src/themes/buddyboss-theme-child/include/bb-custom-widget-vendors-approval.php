<?php

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function vendors_pending_dashboard_widget() {

	wp_add_dashboard_widget(
                 'vendors_pending_approval',         // Widget slug.
                 'Requests to become a vendor',         // Title.
                 'vendors_widget' // Display function.
        );	
}
add_action( 'wp_dashboard_setup', 'vendors_pending_dashboard_widget' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function vendors_widget() {

	$args = array(
    'role'   => 'pending_vendor',
  );

  $users = get_users( $args );
  ob_start();
  ?>

  <?php foreach( $users as $user ): ?>

    <div style="display: flex; align-items: center; margin-bottom: 20px;">
      <?php echo get_avatar( $user->ID, 32 );?>
      <div class="name" style="margin-left: 10px; font-weight: 600;"><?php echo $user->user_login; ?></div>
      <div class="button-approve" >
        <a href="<?php echo get_site_url() . '/wp-admin/users.php?role=pending_vendor&action=approve_vendor&user_id=' . $user->ID; ?>" style="margin-left: auto; background: #0073aa; color: white; padding: 5px 8px; border-radius: 4px;">Approve</a>
      </div>
    </div>

  <?php endforeach; ?>
  <?php
  $contents = ob_get_contents();
  ob_end_clean();
  echo $contents;

}