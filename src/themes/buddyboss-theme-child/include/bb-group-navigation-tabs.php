<?php

function setup_group_shows(){

  global $bp;	
  /* Add some group subnav items */

  $user_access = false;
  $group_link = '';

  if( bp_is_active('groups') && !empty($bp->groups->current_group) ){
    $group_link = $bp->root_domain . '/' . bp_get_groups_root_slug() . '/' . $bp->groups->current_group->slug . '/';
    $user_access = $bp->groups->current_group->user_has_access;
    bp_core_new_subnav_item( array( 
      'name' => __( 'Shows', 'shows'),
      'slug' => 'shows', 
      'parent_url' => $group_link, 
      'parent_slug' => $bp->groups->current_group->slug,
      'screen_function' => 'bp_group_shows', 
      'position' => 50, 
      'user_has_access' => $user_access, 
      'item_css_id' => 'custom' 
    ));
  }
}
add_action( 'bp_init', 'setup_group_shows' );

//Profile Tab
function setup_group_info(){

  global $bp;	
  /* Add some group subnav items */

  $user_access = false;
  $group_link = '';

  if( bp_is_active('groups') && !empty($bp->groups->current_group) ){
    $group_link = $bp->root_domain . '/' . bp_get_groups_root_slug() . '/' . $bp->groups->current_group->slug . '/';
    $user_access = $bp->groups->current_group->user_has_access;
    bp_core_new_subnav_item( array( 
      'name' => __( 'Profile', 'profile'),
      'slug' => 'profile', 
      'parent_url' => $group_link, 
      'parent_slug' => $bp->groups->current_group->slug,
      'screen_function' => 'bp_group_profile', 
      'position' => 30, 
      'user_has_access' => $user_access, 
      'item_css_id' => 'profile' 
    ));
  }
}
add_action( 'bp_init', 'setup_group_info' );

//Music Tab
function setup_group_music(){

  global $bp;	
  /* Add some group subnav items */

  $user_access = false;
  $group_link = '';

  if( bp_is_active('groups') && !empty($bp->groups->current_group) ){
    $group_link = $bp->root_domain . '/' . bp_get_groups_root_slug() . '/' . $bp->groups->current_group->slug . '/';
    $user_access = $bp->groups->current_group->user_has_access;
    bp_core_new_subnav_item( array( 
      'name' => __( 'Music & Streaming', 'music & streaming'),
      'slug' => 'music-and-streaming', 
      'parent_url' => $group_link, 
      'parent_slug' => $bp->groups->current_group->slug,
      'screen_function' => 'bp_group_music', 
      'position' => 55, 
      'user_has_access' => $user_access, 
      'item_css_id' => 'music' 
    ));
  }
}
add_action( 'bp_init', 'setup_group_music' );

//Band Filtering Layout
function bp_group_shows() {
  add_action( 'bp_template_content', 'bp_custom_group_shows' );
  bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function bp_custom_group_shows() {

  get_template_part('templates/bp-group', 'shows');
 
}

//Profile Filtering Layout
function bp_group_profile() {
  add_action( 'bp_template_content', 'bp_custom_group_profile' );
  bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function bp_custom_group_profile() {

  get_template_part('templates/bp-group', 'profile');
 
}


//Music Filtering Layout
function bp_group_music() {
  add_action( 'bp_template_content', 'bp_custom_group_music' );
  bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function bp_custom_group_music() {

  get_template_part('templates/bp-group', 'music');
 
}

