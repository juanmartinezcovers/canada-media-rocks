<?php

add_filter( 'groups_custom_group_fields_editable', 'group_details_markup' );
add_action( 'groups_group_details_edited', 'group_details_save' );
add_action( 'groups_created_group',  'group_details_save' );
 
function group_details_markup() {
  
  global $bp, $wpdb;
  $groupID = $bp->groups->current_group->id;

  //Basic Information
  $hometown = groups_get_groupmeta( $groupID , 'e2e_hometown' );
  $currentLocation = groups_get_groupmeta( $groupID, 'e2e_current-location' );
  $typeOfMusic = groups_get_groupmeta( $groupID, 'e2e_type-of-music' );

  //Band Manager
  $managerCompany = groups_get_groupmeta( $groupID, 'e2e_manager-company' );
  $managerName = groups_get_groupmeta( $groupID, 'e2e_manager-name');
  $managerEmail = groups_get_groupmeta( $groupID, 'e2e_manager-mail' );

  //Booking Manger
  $bookingCompany = groups_get_groupmeta( $groupID, 'e2e_booking-company' );
  $bookingName = groups_get_groupmeta( $groupID, 'e2e_booking-name' );
  $bookingEmail = groups_get_groupmeta( $groupID, 'e2e_booking-mail' );

  //Record Label
  $recordCompany = groups_get_groupmeta( $groupID, 'e2e_record-company');
  $recordName = groups_get_groupmeta( $groupID, 'e2e_record-name' );
  $recordEmail = groups_get_groupmeta( $groupID, 'e2e_record-mail' );

  //Press Contact
  $pressCompany = groups_get_groupmeta( $groupID, 'e2e_press-company');
  $pressName = groups_get_groupmeta( $groupID, 'e2e_press-name' );
  $pressEmail = groups_get_groupmeta( $groupID, 'e2e_press-mail' );

  $youtube = groups_get_groupmeta( $groupID, 'e2e_youtube' );
  $spotify = groups_get_groupmeta( $groupID, 'e2e_spotify' );
  $spotifyPlaylist = groups_get_groupmeta( $groupID, 'e2e_spotify-playlist' );
  $soundcloud = groups_get_groupmeta( $groupID , 'e2e_soundcloud' );
  $apple = groups_get_groupmeta( $groupID, 'e2e_apple' );
  $bandcamp = groups_get_groupmeta( $groupID, 'e2e_bandcamp' );
  $facebook = groups_get_groupmeta( $groupID, 'e2e_facebook' );
  $twitter = groups_get_groupmeta( $groupID, 'e2e_twitter' );
  $instagram = groups_get_groupmeta( $groupID, 'e2e_instagram' );
  $tiktok = groups_get_groupmeta( $groupID, 'e2e_tiktok' );


    ?>

    <h2>Social Media</h2>
    <hr>
    <label for="group-facebook">Facebook Account</label>
    <input type="text" name="group-facebook" id="group-facebook" value="<?php echo $facebook; ?>">

    <label for="group-twitter">Twitter Account</label>
    <input type="text" name="group-twitter" id="group-twitter" value="<?php echo $twitter; ?>">

    <label for="group-instagram">Instagram Account </label>
    <input type="text" name="group-instagram" id="group-instagram" value="<?php echo $instagram; ?>">

    <label for="group-tiktok">Tik Tok Account</label>
    <input type="text" name="group-tiktok" id="group-tiktok" value="<?php echo $tiktok; ?>">

    <h2>Location</h2>
    <hr>

    <label for="group-hometown">Hometown</label>
    <input type="text" id="group-hometown" name="group-hometown" value="<?php  echo $hometown; ?>">

    <label for="group-current-location">Current Location</label>
    <input type="text" id="group-current-location" name="group-current-location" value="<?php echo $currentLocation;?>">

    <hr>
    <h2>What type of music do you play?</h2>
    <input type="text" name="group-type-of-music" id="group-type-of-music" value="<?php echo $typeOfMusic ?>">
    <hr>
    <h2>Favorite Types of Music</h2>


    <?php 
      $genres = array(
        'Rock n Roll',
        'Indie Rock',
        'Classic Rock',
        'Alternative Rock',
        'Rock a Billy',
        'Pop Rock',
        'Glam Rock',
        'Surf Rock',
        'Garage Rock',
        'Root Rock',
        'Progressive Rock',
        'Punk Rock',
        'New Wave',
        'Post Punk',
        'Heavy Metal',
        'Hair Metal',
        'Scream Metal',
        'Goth',
        'Grunge',
        'Post Grunge',
        'Folk',
        'Folk Rock',
        'Blues',
        'Blues Rock',
        'Jazz',
        'Jazz Rock',
        'Swing Jazz'
      );

      foreach ( $genres as $genre ):
        $handler = str_replace( ' ', '-', strtolower( $genre ) );
    ?>
      <div class="d-flex genre-checkbox <?php echo $handler ?>">
      <input type="checkbox" name="group-<?php echo $handler ?>" id="group-<?php echo $handler ?>" value="<?php echo $genre ?>" <?php if ( groups_get_groupmeta( $groupID, 'e2e_' . $handler ) === $genre ): ?> checked <?php endif; ?>>
        <label for="group-<?php echo $handler ?>"><?php echo $genre ?></label>
      </div>
    <?php  endforeach; ?>
    
    <h2 class="mt-3">Band Manager</h2>
    <label for="group-manager-company">Company Name</label>
    <input type="text" name="group-manager-company" id="group-manager-company" value="<?php echo $managerCompany; ?>" >
    <label for="group-manager-name">Manager Name</label>
    <input type="text" name="group-manager-name" id="group-manager-name" value="<?php echo $managerName; ?>">
    <label for="group-manager-mail">Manager Email</label>
    <input type="email" name="group-manager-mail" id="group-manager-mail" value="<?php echo $managerEmail; ?>">


    <hr>
    <h2>Booking Manager</h2>
    <label for="group-booking-company">Booking Company</label>
    <input type="text" name="group-booking-company" id="group-booking-company" value="<?php echo $bookingCompany; ?>" >
    <label for="group-booking-name">Contact Name</label>
    <input type="text" name="group-booking-name" id="group-booking-name" value="<?php echo $bookingName; ?>">
    <label for="group-booking-mail">Contact Email</label>
    <input type="email" name="group-booking-mail" id="group-booking-mail" value="<?php echo $bookingEmail; ?>">

    <hr>
    <h2>Record Label</h2>
    <label for="group-record-company">Label Name</label>
    <input type="text" name="group-record-company" id="group-record-company" value="<?php echo $recordCompany; ?>" >
    <label for="group-record-name">Contact Name</label>
    <input type="text" name="group-record-name" id="group-record-name" value="<?php echo $recordName; ?>">
    <label for="group-record-mail">Contact Email</label>
    <input type="email" name="group-record-mail" id="group-record-mail" value="<?php echo $recordEmail; ?>">
    
    <hr>
    <h2>Press Contact</h2>
    <label for="group-press-company">Company Name</label>
    <input type="text" name="group-press-company" id="group-press-company" value="<?php echo $pressCompany; ?>" >
    <label for="group-press-name">Contact Name</label>
    <input type="text" name="group-press-name" id="group-press-name" value="<?php echo $pressName; ?>">
    <label for="group-press-mail">Contact Email</label>
    <input type="email" name="group-press-mail" id="group-press-mail" value="<?php echo $pressEmail; ?>">
    
    <hr>
    <h2>Music Download & Streaming Services</h2>

    <label for="group-youtube">Youtube Channel URL</label>
    <input type="url" name="group-youtube" id="group-youtube" value="<?php echo $youtube; ?>" class="mb-3">
    <label for="group-spotify">Spotify Artist URI or URL</label>
    <input type="url" name="group-spotify" id="group-spotify" value="<?php echo $spotify; ?>" class="mb-3">
    <label for="group-spotify-playlist">Spotify Playlist URL</label>
    <input type="url" name="group-spotify-playlist" id="group-spotify-playlist" value="<?php echo $spotifyPlaylist; ?>" class="mb-3">
    <label for="group-soundcloud">SoundCloud Playlist URL</label>
    <input type="text" name="group-soundcloud" id="group-soundcloud" value="<?php echo $soundcloud; ?>" class="mb-3">
    <label for="group-apple">Apple Playlist URL</label>
    <input type="text" name="group-apple" id="group-apple" value="<?php echo $apple ?>" class="mb-3">
    <label for="group-bandcamp">Bandcamp URL</label>
    <input type="text" name="group-bandcamp" id="group-bandcamp" value="<?php echo $bandcamp; ?>">

    <?php if( current_user_can('administrator') ): 
      $bands = tribe_get_organizers();
    ?>
      <h2>Events</h2>
      <hr>
      <label for="group-band-ec">Link this band to events calendar:</label>

      <select name="group-band-ec" id="group-band-ec" class="mb-3">
        <option value="empty"> ---------- </option>
      <?php 
      $selected = groups_get_groupmeta( $groupID, 'e2e_band-ec' );
      foreach( $bands as $band ): 
        setup_postdata( $band );
        $bandID = $band->ID; ?>
        <option value="<?php echo $bandID; ?>"  <?php if( strval($bandID) === strval($selected) ): ?> selected <?php endif; ?>> <?php echo $band->post_title; ?> </option>
      <?php endforeach; ?>
      </select>
    <?php endif;?>
    <?php return;
}
 
function group_details_save( $group_id ) {
    global $bp, $wpdb;

    $genres = array(
      'Rock n Roll',
      'Indie Rock',
      'Classic Rock',
      'Alternative Rock',
      'Rock a Billy',
      'Pop Rock',
      'Glam Rock',
      'Surf Rock',
      'Garage Rock',
      'Root Rock',
      'Progressive Rock',
      'Punk Rock',
      'New Wave',
      'Post Punk',
      'Heavy Metal',
      'Hair Metal',
      'Scream Metal',
      'Goth',
      'Grunge',
      'Post Grunge',
      'Folk',
      'Folk Rock',
      'Blues',
      'Blues Rock',
      'Jazz',
      'Jazz Rock',
      'Swing Jazz'
    );
 
    $plain_fields = array(
        'hometown',
        'current-location',
        'type-of-music',
        $genres,
        'manager-company',
        'manager-name',
        'manager-mail',
        'booking-company',
        'booking-name',
        'booking-mail',
        'record-company',
        'record-name',
        'record-mail',
        'press-company',
        'press-name',
        'press-mail',
        'facebook',
        'twitter',
        'instagram',
        'tiktok',
        'band-ec',
        'spotify',
        'spotify-playlist',
        'soundcloud',
        'youtube',
        'apple',
        'bandcamp'
    );
 
 
    foreach( $plain_fields as $field ) {

      if ( is_array( $field ) ){
        foreach( $field as $inner ){
          $string = str_replace( ' ', '-', strtolower( $inner ) );
          $innerKey = 'group-' . $string;

          if ( isset( $_POST[$innerKey] ) ){
            $innerValue = $_POST[$innerKey];
            groups_update_groupmeta( $group_id, 'e2e_' . $string, $innerValue );
          }
        }
      }

      $key = 'group-' . $field;
      if ( isset( $_POST[$key] ) ) {
          $value = $_POST[$key];
          groups_update_groupmeta( $group_id, 'e2e_' . $field, $value );
      }
    }
}


function extra_fields_output(){
  
  global $bp, $wpdb;
  $groupID = $bp->groups->current_group->id;
  $typeOfMusic = groups_get_groupmeta( $groupID, 'e2e_type-of-music' );
  $facebook = groups_get_groupmeta( $groupID, 'e2e_facebook' );
  $twitter = groups_get_groupmeta( $groupID, 'e2e_twitter' );
  $instagram = groups_get_groupmeta( $groupID, 'e2e_instagram' );
  $tiktok = groups_get_groupmeta( $groupID, 'e2e_tiktok' );

  ?>
    <div class="type-of-music">
      <?php echo $typeOfMusic; ?>
    </div>

  <div class="social-networks-wrap mt-3">
  <?php  if( !empty( $facebook ) ): ?>
   <span class="social facebook m-0">
      <a target="_blank" data-balloon-pos="up" data-balloon="Facebook" href="<?php echo $facebook; ?>">
         <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
            <path fill="#333" d="M16 0c-8.8 0-16 7.2-16 16s7.2 16 16 16c8.8 0 16-7.2 16-16s-7.2-16-16-16v0zM20.192 10.688h-1.504c-1.184 0-1.376 0.608-1.376 1.408v1.792h2.784l-0.384 2.816h-2.4v7.296h-2.912v-7.296h-2.496v-2.816h2.496v-2.080c-0.096-2.496 1.408-3.808 3.616-3.808 0.992 0 1.888 0.096 2.176 0.096v2.592z"></path>
         </svg>
      </a>
   </span>
  <?php endif; ?>

  <?php  if( !empty( $instagram ) ): ?>
   <span class="social instagram m-0">
      <a target="_blank" data-balloon-pos="up" data-balloon="Instagram" href="<?php echo $instagram; ?>">
         <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
            <path fill="#333" d="M16 19.104c-1.696 0-3.104-1.408-3.104-3.104 0-1.728 1.408-3.104 3.104-3.104 1.728 0 3.104 1.376 3.104 3.104 0 1.696-1.376 3.104-3.104 3.104zM19.616 12.896c-0.32 0-0.512-0.192-0.416-0.384v-2.208c0-0.192 0.192-0.416 0.416-0.416h2.176c0.224 0 0.416 0.224 0.416 0.416v2.208c0 0.192-0.192 0.384-0.416 0.384h-2.176zM16 0c-8.8 0-16 7.2-16 16s7.2 16 16 16c8.8 0 16-7.2 16-16s-7.2-16-16-16v0zM24 22.112c0 0.992-0.896 1.888-1.888 1.888h-12.224c-0.992 0-1.888-0.8-1.888-1.888v-12.224c0-1.088 0.896-1.888 1.888-1.888h12.224c0.992 0 1.888 0.8 1.888 1.888v12.224zM20.896 16c0 2.688-2.208 4.896-4.896 4.896s-4.896-2.208-4.896-4.896c0-0.416 0.096-0.896 0.192-1.312h-1.504v7.008c0 0.192 0.224 0.416 0.416 0.416h11.488c0.192 0 0.416-0.224 0.416-0.416v-7.008h-1.504c0.192 0.416 0.288 0.896 0.288 1.312z"></path>
         </svg>
      </a>
   </span>
  <?php endif; ?>

  <?php  if( !empty( $twitter ) ): ?>
   <span class="social twitter m-0">
      <a target="_blank" data-balloon-pos="up" data-balloon="Twitter" href="<?php echo $twitter; ?>">
         <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
            <path fill="#333" d="M16 0c-8.8 0-16 7.2-16 16s7.2 16 16 16c8.8 0 16-7.2 16-16s-7.2-16-16-16v0zM22.4 12.704v0.384c0 4.32-3.296 9.312-9.312 9.312-1.888 0-3.584-0.512-4.992-1.504h0.8c1.504 0 3.008-0.512 4.096-1.408-1.376 0-2.592-0.992-3.104-2.304 0.224 0 0.416 0.128 0.608 0.128 0.32 0 0.608 0 0.896-0.128-1.504-0.288-2.592-1.6-2.592-3.2v0c0.416 0.224 0.896 0.416 1.504 0.416-0.896-0.608-1.504-1.6-1.504-2.688 0-0.608 0.192-1.216 0.416-1.728 1.6 2.016 4 3.328 6.784 3.424-0.096-0.224-0.096-0.512-0.096-0.704 0-1.792 1.504-3.296 3.296-3.296 0.896 0 1.792 0.384 2.4 0.992 0.704-0.096 1.504-0.416 2.112-0.8-0.224 0.8-0.8 1.408-1.408 1.792 0.704-0.096 1.312-0.288 1.888-0.48-0.576 0.8-1.184 1.376-1.792 1.792v0z"></path>
         </svg>
      </a>
   </span>
  <?php  endif; ?>

  <?php  if( !empty( $tiktok ) ): ?>
   <span class="social tiktok m-0">
      <a target="_blank" data-balloon-pos="up" data-balloon="TikTok" href="<?php echo $tiktok; ?>">
        <?php get_template_part( 'icons/icon', 'tiktok' ); ?>
      </a>
   </span>
  <?php  endif; ?>
  </div>

  <?php
}
add_action( 'bp_group_header_meta', 'extra_fields_output'  );