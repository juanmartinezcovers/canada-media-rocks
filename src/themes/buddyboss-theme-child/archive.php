<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BuddyBoss_Theme
 */

get_header();

$blog_type = 'standard'; // standard, grid, masonry.
$blog_type = apply_filters( 'bb_blog_type', $blog_type );

$class = '';

if ( 'masonry' === $blog_type ) {
	$class = 'bb-masonry';
} elseif ( 'grid' === $blog_type ) {
	$class = 'bb-grid';
} else {
	$class = 'bb-standard';
}

$term_id = get_queried_object()->term_id;
$featured_image = get_term_meta( $term_id, 'bb_file_field_id', true );
?>

<style>
  .page-header-archive {
    background: url("<?php echo $featured_image["url"]; ?>");
    background-size: cover;
    background-position: center;
  }
</style>
			

<div id="primary" class="content-area">
	<main id="main" class="site-main">    

		<?php if ( have_posts() ) : ?>
			<div class="post-grid <?php echo esc_attr( $class ); ?>">
				<?php if ( 'masonry' === $blog_type ) { ?>
					<div class="bb-masonry-sizer"></div>
				<?php } ?>
         <div class="row align-items-center">
          <?php
          /* Start the Loop */
          while ( have_posts() ) :
            the_post(); ?>
              <div class="col-md-4">
              <?php
            /*
            * Include the Post-Format-specific template for the content.
            * If you want to override this in a child theme, then include a file
            * called content-___.php (where ___ is the Post Format name) and that will be used instead.
            */
            get_template_part( 'template-parts/content', apply_filters( 'bb_blog_content', get_post_format() ) ); ?>
            </div>
          <?php endwhile;
          ?>
         </div>
			</div>

			<?php
			buddyboss_pagination();

		else :
			get_template_part( 'template-parts/content', 'none' );
			?>

		<?php endif; ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php // get_sidebar(); ?>

<?php
get_footer();
