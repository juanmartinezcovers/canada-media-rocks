jQuery(document).ready(function($){

  //Login Form
	$('.tribe-common-c-btn--small').attr("href", "").attr({ 'data-toggle': 'modal', 'data-target': '#login-form' });	
	$('.login-username').addClass('d-flex flex-column');
	$('.login-password').addClass('d-flex flex-column');
  $('.login-submit').css('text-align', 'right');

  //Hide Metadata
  $('.tribe-events-single-section-title').last().parent().hide();
});
