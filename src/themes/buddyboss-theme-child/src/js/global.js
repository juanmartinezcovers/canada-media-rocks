jQuery(document).ready(function($){
  const body = $('body');
  const mainLogo = $('.site-title a');

  if (body.hasClass('logged-in')){
    mainLogo.attr('href', '/news-feed');
  }
});