jQuery(document).ready(function($){

  // $('#content').children().children().removeClass('bb-grid');

  const sliderHero = $('.slider-hero');
  sliderHero.slick({
    dots: false,
    arrows: false,
    fade: true,
    autoplay: true,
    autoplaySpeed: 3500,
    cssEase: 'linear'
  });

  $('#content').children().removeClass('container');

  let sliderNav = $('.slider-nav-col');
  sliderNav.each(function(index){
      $(this).on("click", function(){
          sliderHero.slick('slickGoTo', index);
      });
  });

  let posts = $('.tab-posts');
  let tabs = $('.category');
  tabs.each(function(index){
    $(this).on("click", function(event){
      tabs.removeClass('active-tab');
      $(this).addClass('active-tab');
      $('.load-more').attr('data-page', function(i, val){
        return 1;
      });

      if ($('.load-more').text() === "Ups, we don't have more post on this category" ){
        $('.load-more').text('Load More');
      }
  
      let category = $(this).data('tabLink');
      let data = {
        action: 'tab_magazine',
        category: category,
      }
  
      $.ajax({
        url: myAjax.ajaxurl,
        type: 'post',
        data: data,
        beforeSend: function(){
          posts.html('<div class="preloader-magazine col-md-6 col-12 mx-auto my-3"></div>');
        },
        success: function( response ) {
          $('.load-more').attr("data-max-page", function(i, val){
            return response[1];
          });
          posts.fadeOut(200, function(){
            $(this).html(response[0]);
          }).fadeIn(1000);
        }
      });
    })
  });

  let loadMore = $('.load-more');
  let ajaxLoad = $('.ajax-load');
  loadMore.on("click", function(event){

    let maxPages = parseInt($(this).attr('data-page'));
    let paged = parseInt($(this).attr('data-max-page'));

    if (maxPages === paged){
      $(this).text("Ups, we don't have more post on this category"); 
      return;
    }

    let activeTab = $('.active-tab');
    let category = activeTab.data('tabLink');
    $(this).attr('data-page', function(i, val){
      let increase = parseInt(val) + 1;
      return increase;
    });

    let page = parseInt($(this).attr('data-page'));
    let data = {
      action: 'load_more_magazine',
      category: category,
      page: page
    }


    $.ajax({
      url: myAjax.ajaxurl,
      type: 'post',
      data: data,
      beforeSend: function(){
        ajaxLoad.addClass('ajax-loading');
      },
      success: function( response ) {
        ajaxLoad.removeClass('ajax-loading');
        loadMore.attr('data-page', page);
        posts.append(response);
      }
    });

  });

  let videoParent = $('.videos-parent-container');
  let videoNav = $('.videos-child-container');

  videoParent.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: videoNav
  });

  videoNav.slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: videoParent,
    autoplay: true,
    autoplaySpeed: 3500,
    dots: true,
    centerPadding: '20px',
    centerMode: true,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  
  function validate(){
      let mail = $('.newsletters #mail').val();
      let submit = $('.newsletters input[type="submit"]');
      let border = $('.input-mail');
      let envelope = $('.newsletters .bi-envelope');
      if( validateEmail(mail) ){
          envelope.css("fill", "green");
          border.css("border-color", "green");
          submit.attr("disabled", false);
      } else {
          submit.attr("disabled", true);
          envelope.css("fill", "gray");
          border.css("border-color", "gray");
  
      }
  }
  
  $('#mail').keyup(validate);
});