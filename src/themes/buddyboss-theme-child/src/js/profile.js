jQuery(document).ready(function($){

  //Selecting repeater fields for bands/musicians
  let favBands = $('.favorite-bands-musicians .repeater_set_title');
  for(let i = 0; i < favBands.length; i++){
    if( ! favBands[i].innerText.length > 0 ){
      favBands[i].innerText = 'Choose your favorite band/musician';
    } else {
      order = i + 1;
      favBands[i].innerText = 'Favorite Band/Musicia ' + order;
    }
  }

  //Selecting repeater fields for genre
  let favGenre = $('.favorite-music-genre .repeater_set_title');
  for (let i = 0; i < favGenre.length; i++ ){
    if( favGenre[i].innerText === 'public' ){
      favGenre[i].innerText = 'Choose your favorite music genre';
    }
  }
  
  let instrument = $('.instruments-played .repeater_set_title');
  for (let i = 0; i < instrument.length; i++){
    if (! instrument[i].innerText.length > 0){
      instrument[i].innerText = 'What instrument do you play?';
    }
  }

  let band = $('.band .repeater_set_title');
  for (let i = 0; i < band.length; i++){
    if( ! band[i].innerText.length > 0 ){
      band[i].innerText = 'Choose your band in the dropdown below';
    } 
  }

  //Hiding Press Contact (Only for UX purpouses)
  let pressField = $('#field_245');
  let pressFieldPublic = $('#field-visibility-settings-toggle-245');
  let pressFieldTitle = $('#field_245-1');

  pressField.hide();
  pressFieldPublic.hide();
  pressFieldTitle.css({
    "font-size": "20px",
    "font-weight": "500",
    "color": "black"
  });
  pressFieldTitle.append('<hr style="margin: 10px 0 0 0;" >');

  //Hiding spotify playlists in profile global information for musicians
  let spotifyPlayPublic = $('.profile .public .spotify-playlist');
  spotifyPlayPublic.hide();

  //Youtube Slider for Video Component
  let youtubeSlider = $('.youtube-videos-slider');
});