<?php
/**
 * @package BuddyBoss Child
 * The parent theme functions are located at /buddyboss-theme/inc/theme/functions.php
 * Add your own functions at the bottom of this file.
 */


/****************************** THEME SETUP ******************************/

/**
 * Sets up theme for translation
 *
 * @since BuddyBoss Child 1.0.0
 */

require_once( __DIR__ . '/include/bb-social-addon.php');
require_once( __DIR__ . '/include/bb-profile-navigation-tabs.php');
require_once( __DIR__ . '/magazine/includes/tabs-ajax.php' );
require_once( __DIR__ . '/magazine/includes/load-more.php' );
require_once( __DIR__ . '/include/bb-group-navigation-tabs.php' );
require_once( __DIR__ . '/include/bb-group-custom-fields.php' );
require_once(__DIR__ . '/vendors/opengraph-helper.php');
require_once(__DIR__ . '/include/bb-custom-widget-vendors-approval.php');


function buddyboss_theme_child_languages()
{
  /**
   * Makes child theme available for translation.
   * Translations can be added into the /languages/ directory.
   */

  // Translate text from the PARENT theme.
  load_theme_textdomain( 'buddyboss-theme', get_stylesheet_directory() . '/languages' );

  // Translate text from the CHILD theme only.
  // Change 'buddyboss-theme' instances in all child theme files to 'buddyboss-theme-child'.
  // load_theme_textdomain( 'buddyboss-theme-child', get_stylesheet_directory() . '/languages' );

}
add_action( 'after_setup_theme', 'buddyboss_theme_child_languages' );

/**
 * Enqueues scripts and styles for child theme front-end.
 *
 * @since Boss Child Theme  1.0.0
 */
function buddyboss_theme_child_scripts_styles()
{
  /**
   * Scripts and Styles loaded by the parent theme can be unloaded if needed
   * using wp_deregister_script or wp_deregister_style.
   *
   * See the WordPress Codex for more information about those functions:
   * http://codex.wordpress.org/Function_Reference/wp_deregister_script
   * http://codex.wordpress.org/Function_Reference/wp_deregister_style
   **/

  //Only For Events Page
  if ( is_single() && get_post_type() === 'tribe_events'  ){
    wp_enqueue_script( 'events-canada', get_stylesheet_directory_uri() . '/dist/js/events.js' , array('jquery') , '1.0.0', true );
  }

  //Only for profile page
  if ( bp_is_my_profile() ){
    wp_enqueue_script('profile-custom-js', get_stylesheet_directory_uri() . '/dist/js/profile.js', array('jquery'), '1.0.0'. true);
  }

  //Bootstrap CSS
  wp_enqueue_script('plugins-min', get_stylesheet_directory_uri() . '/dist/js/plugins/plugins.min.js', array('jquery'), '1.0.0', true);
  wp_enqueue_script( 'global-js', get_stylesheet_directory_uri() . '/dist/js/global.js', array('jquery'), '1.0.0', true );

  if( !is_page( array('checkout', 'cart', 'my-account') ) ){
    wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/dist/css/bootstrap.min.css' );
  }

  if ( bp_is_user() or bp_is_groups_component() ){
    wp_enqueue_style('events-calendar', '/wp-content/plugins/the-events-calendar/common/src/resources/css/common-full.min.css');
    wp_enqueue_style('views-full-events-calendar', '/wp-content/plugins/the-events-calendar/src/resources/css/views-full.min.css');
    wp_enqueue_style( 'skeleton-events-calendar', '/wp-content/plugins/the-events-calendar/src/resources/css/views-skeleton.min.css' );
    wp_enqueue_style( 'views-skeleton', '/wp-content/plugins/the-events-calendar/src/resources/css/views-skeleton.min.css' );
  }

  wp_enqueue_style('seerocklive-css', get_stylesheet_directory_uri() . '/dist/css/style.css');

  //Group Page only
  if( bp_is_groups_component() ){
    wp_enqueue_script('group', get_stylesheet_directory_uri() . '/dist/js/groups.js', array('jquery'), '1.0.0', true );
  }
  //Bootstrap JS
  wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() . '/dist/js/plugins/plugins.min.js', array('jquery') , '' ,false );

}
add_action( 'wp_enqueue_scripts', 'buddyboss_theme_child_scripts_styles', 9999 );

function wp_login_custom_css(){
  wp_enqueue_style( 'login-custom-styles', get_stylesheet_directory_uri() . '/dist/css/login-custom.css' );
}
add_action('login_enqueue_scripts','wp_login_custom_css');

function add_video_login(){
  ?>
    <video playsinline autoplay muted loop id="bgvid">
      <source src="<?php echo get_stylesheet_directory_uri() . '/login-video/login_video.mp4' ?>" type="video/mp4">
    </video>
  <?php
}
add_action('login_header', 'add_video_login');


add_action('wp_enqueue_scripts', 'magazine_ajax');
function magazine_ajax(){

  if( is_page('magazine') ) {
    wp_register_script( 'magazine-js', get_stylesheet_directory_uri() . '/dist/js/magazine.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script('magazine-js');
    wp_localize_script('magazine-js', 'myAjax', array(
      'ajaxurl' => admin_url( 'admin-ajax.php' )
   ));
  }
}

//Register Magazine Custom Menu Position
function magazine_menu(){
  register_nav_menu( 'magazine', __( 'Magazine Menu' ) );
}
add_action('init', 'magazine_menu');

// Featured Post option for Theme 
function bb_custom_meta() {
  add_meta_box( 'bb_featured', __( 'Featured Posts', 'bb-textdomain' ), 'bb_meta_callback', 'post', 'side', 'high' );
}

function bb_meta_callback( $post ) {
  $featured = get_post_meta( $post->ID );
  ?>

<p>
  <div class="bb-row-content">
      <label for="meta-checkbox">
          <input type="checkbox" name="meta-checkbox" id="meta-checkbox" value="yes" <?php if ( isset ( $featured['meta-checkbox'] ) ) checked( $featured['meta-checkbox'][0], 'yes' ); ?> />
          <?php _e( 'Featured this post', 'bb-textdomain' )?>
      </label>
      
  </div>
</p>

  <?php
}
add_action( 'add_meta_boxes', 'bb_custom_meta' );

function bb_meta_save( $post_id ) {
 
  // Checks save status
  $is_autosave = wp_is_post_autosave( $post_id );
  $is_revision = wp_is_post_revision( $post_id );
  $is_valid_nonce = ( isset( $_POST[ 'bb_nonce' ] ) && wp_verify_nonce( $_POST[ 'bb_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

  // Exits script depending on save status
  if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
      return;
  }

// Checks for input and saves
if( isset( $_POST[ 'meta-checkbox' ] ) ) {
  update_post_meta( $post_id, 'meta-checkbox', 'yes' );
} else {
  update_post_meta( $post_id, 'meta-checkbox', '' );
}

}
add_action( 'save_post', 'bb_meta_save' );

function var_view( $data ){
  echo '<pre>' . var_export($data, true) . '</pre>';
}

function wpb_set_post_views($postID) {
  $count_key = 'wpb_post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
      $count = 0;
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
  }else{
      $count++;
      update_post_meta($postID, $count_key, $count);
  }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
  if ( !is_single() ) return;
  if ( empty ( $post_id) ) {
      global $post;
      $post_id = $post->ID;    
  }
  wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

// Creating the widget 
class bb_most_viewed extends WP_Widget {
  
  function __construct() {
  parent::__construct(
    
  // Base ID of your widget
  'bb_most_viewed', 
    
  // Widget name will appear in UI
  __('Most Viewed Posts Widget', 'bb_most_viewed_domain'), 
    
  // Widget description
  array( 'description' => __( 'Display most viewed posts', 'bb_most_viewed_domain' ), ) 
  );
  }
    
  // Creating widget front-end
    
  public function widget( $args, $instance ) {
  $title = apply_filters( 'widget_title', $instance['title'] );
    
  // before and after widget arguments are defined by themes
  echo $args['before_widget'];
  if ( ! empty( $title ) )
  echo $args['before_title'] . $title . $args['after_title'];
    
  // This is where you run the code and display the output
  get_template_part( 'magazine/includes/most-viewed', 'posts' ); 
  echo $args['after_widget'];
  }
            
  // Widget Backend 
  public function form( $instance ) {

    if ( isset( $instance[ 'title' ] ) ) {
    $title = $instance[ 'title' ];
    }
    else {
    $title = __( 'New title', 'bb_most_viewed_domain' );
    }
    // Widget admin form
    ?>
    <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <?php 

  }
        
  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
  }
   
  // Class bb_most_viewed ends here
} 
   
// Register and load the widget
function wpb_load_widget() {
    register_widget( 'bb_most_viewed' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

//Render shortcodes on widgets
add_filter( 'widget_text', 'do_shortcode' );

// Redirecting on login
function redirect_users() {
  if ( ! is_admin() ){
    wp_redirect( '/news-feed' );
    exit;
  }
}
add_action('wp_login', 'redirect_users');


//Redirect on First Login
function bp_redirect_on_first_login( $redirect_to, $redirect_url_specified, $user ) {
 
  //check if we have a valid user?
  if ( is_wp_error( $user ) ) {
      return $redirect_to;
  }

  //check for user's last activity
  $last_activity =  bp_get_user_last_activity( $user->ID );

  if ( empty( $last_activity ) ) {
      //it is the first login
      //update redirect url
      //I am redirecting to user's profile here
      //you may change it to anything
      $redirect_to = bp_core_get_user_domain($user->ID );
  }

  return $redirect_to;
}

add_filter( 'login_redirect', 'bp_redirect_on_first_login', 110, 3 );