<?php
/**
 * Template Name: Magazine
 * @package BuddyBoss_Theme
 */

 get_header();
?>

<?php 

  get_template_part( 'magazine/slider' );  
  get_template_part( 'magazine/tabs' );
  get_template_part( 'magazine/videos' );
  get_template_part( 'magazine/recent', 'posts' );
  get_template_part( 'magazine/column', 'categories' );
  get_template_part( 'magazine/newsletters' );

?>

<?php
get_footer();