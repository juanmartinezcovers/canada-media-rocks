<div class="tribe-common tribe-events tribe-events-view tribe-events-view--organizer tribe-events-view--list tribe-events-pro tribe-common--breakpoint-xsmall tribe-common--breakpoint-medium tribe-common--breakpoint-full" data-featured="<?php if(Tribe__Events__Featured_Events::is_featured( $post )):?>true<?php else: ?>false<?php endif; ?>">
  <div class="tribe-events-calendar-list">
    <div class="tribe-common-g-row tribe-events-calendar-list__event-row tribe-events-calendar-list__event-row--featured">
      <div class="tribe-events-calendar-list__event-date-tag tribe-common-g-col">
        <time class="tribe-events-calendar-list__event-date-tag-datetime" datetime="<?php echo tribe_get_start_date($post->ID, false, 'Y-m-d'); ?>">
          <span class="tribe-events-calendar-list__event-date-tag-weekday"><?php echo tribe_get_start_date($post->ID, false, 'D'); ?></span>
          <span class="tribe-events-calendar-list__event-date-tag-daynum tribe-common-h5 tribe-common-h4--min-medium"><?php echo tribe_get_start_date($post->ID, false, 'd'); ?></span>
        </time>
      </div>
      <div class="tribe-events-calendar-list__event-wrapper tribe-common-g-col">
        <article class="tribe-events-calendar-list__event tribe-common-g-row tribe-common-g-row--gutters post-<?php echo $post->ID; ?> tribe_events type-tribe_events status-publish has-post-thumbnail hentry search-hentry">
          <div class="tribe-events-calendar-list__event-featured-image-wrapper tribe-common-g-col">
            <a href="<?php the_permalink();  ?>" title="<?php echo $post->post_title; ?>" rel="bookmark" class="tribe-events-calendar-list__event-featured-image-link">
              <img src="<?php echo tribe_event_featured_image($post->ID, 'full', false, false); ?>" alt="" class="tribe-events-calendar-list__event-featured-image">
            </a>
          </div>
          <div class="tribe-events-calendar-list__event-details tribe-common-g-col">
            <header class="tribe-events-calendar-list__event-header">
              <div class="tribe-events-calendar-list__event-datetime-wrapper tribe-common-b2">
                <?php if( Tribe__Events__Featured_Events::is_featured( $post ) ):?>
                <em class="tribe-events-calendar-list__event-datetime-featured-icon tribe-common-svgicon tribe-common-svgicon--featured" aria-label="Featured" title="Featured"></em>
                <span class="tribe-events-calendar-list__event-datetime-featured-text tribe-common-a11y-visual-hide">Featured</span> 
                <?php endif; ?>
                <time class="tribe-events-calendar-list__event-datetime" datetime="<?php echo tribe_get_start_date($post->ID, true ); ?>">
                  <span class="tribe-event-date-start"><?php echo tribe_get_start_date($post->ID, true); ?></span>  - <span class="tribe-event-time"><?php echo tribe_get_end_time($post->ID); ?></span>
                </time>
              </div>
              <h3 class="tribe-events-calendar-list__event-title tribe-common-h6 tribe-common-h4--min-medium"><a href="<?php the_permalink(); ?>" rel="bookmark" class="tribe-events-calendar-list__event-title-link tribe-common-anchor-thin" title="<?php echo $post->post_title; ?>"><?php echo $post->post_title; ?></a></h3>
              <address class="tribe-events-calendar-list__event-venue tribe-common-b2">
                <span class="tribe-events-calendar-list__event-venue-title tribe-common-b2--bold"><?php echo tribe_get_venue( $post->ID ); ?></span>
                <span class="tribe-events-calendar-list__event-venue-address"><?php echo tribe_get_address( $post->ID ); ?>, <?php echo tribe_get_city( $post->ID ); ?></span>
              </address>
            </header>
            <div class="tribe-events-calendar-list__event-description tribe-common-b2 tribe-common-a11y-hidden"><?php the_excerpt(); ?></div>
          </div>
        </article>
      </div>
    </div>
  </div>
</div>
