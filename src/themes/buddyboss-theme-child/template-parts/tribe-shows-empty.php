<div class="tribe-common tribe-events tribe-events-view tribe-events-view--organizer tribe-events-view--list tribe-events-pro tribe-common--breakpoint-xsmall tribe-common--breakpoint-medium tribe-common--breakpoint-full" >
   <div class="tribe-common-l-container-- tribe-events-l-container--">
      <div class="tribe-events-view-loader tribe-common-a11y-hidden" role="alert" aria-live="assertive">
         <div class="tribe-events-view-loader__dots tribe-common-c-loader">
            <div class="tribe-common-c-loader__dot tribe-common-c-loader__dot--first"></div>
            <div class="tribe-common-c-loader__dot tribe-common-c-loader__dot--second"></div>
            <div class="tribe-common-c-loader__dot tribe-common-c-loader__dot--third"></div>
         </div>
      </div>
      
      <header class="tribe-events-header tribe-events-header--has-event-search">
         <div class="tribe-events-header__messages tribe-events-c-messages tribe-common-b2">
            <div class="tribe-events-c-messages__message tribe-events-c-messages__message--notice" role="alert">
               <ul class="tribe-events-c-messages__message-list">
                  <li class="tribe-events-c-messages__message-list-item">
                     There were no results found.					
                  </li>
               </ul>
            </div>
         </div>
      </header>
   </div>
</div>