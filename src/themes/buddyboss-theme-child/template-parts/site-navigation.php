<nav id="site-navigation" class="main-navigation" data-menu-space="120">
    <div id="primary-navbar">
    	<?php
      
      
      if ( is_buddypress() or !is_page( 'magazine' ) ){
        wp_nav_menu( array(
          'theme_location' => 'header-menu',
          'menu_id'		 => 'primary-menu',
          'container'		 => false,
          'fallback_cb'	 => '',
          'menu_class'	 => 'primary-menu bb-primary-overflow', )
        );
      } else {
        wp_nav_menu( array(
          'theme_location'  => 'magazine',
          'menu_id'         => 'magazine-menu',
          'container'       => false,
          'fallback_cb'     => '',
          'menu_class'      => 'primary-menu bb-primary-overflow magazine-menu'
          ) 
        );
      }
    	?>
        <div id="navbar-collapse">
            <a class="more-button" href="#"><i class="bb-icon-menu-dots-h"></i></a>
            <ul id="navbar-extend" class="sub-menu"></ul>
        </div>
    </div>
</nav>