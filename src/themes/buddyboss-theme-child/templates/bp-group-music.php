<?php 

  
  /**
   * Get Group ID
   */
  global $bp, $wpdb;
  $groupID = $bp->groups->current_group->id;

  /**
   * Fetching fields+
   */

  $youtube = groups_get_groupmeta( $groupID, 'e2e_youtube' );
  $spotify = groups_get_groupmeta( $groupID, 'e2e_spotify' );
  $spotifyPlaylist = groups_get_groupmeta( $groupID, 'e2e_spotify-playlist' );
  $apple = groups_get_groupmeta( $groupID, 'e2e_apple' );
  $soundcloud = groups_get_groupmeta( $groupID , 'e2e_soundcloud' );
  $bandcamp = groups_get_groupmeta( $groupID, 'e2e_bandcamp' );

  function soundcloudEmbed( $link ){

    $url = filter_var( $link, FILTER_SANITIZE_URL );
    if( filter_var( $url, FILTER_VALIDATE_URL ) == true ){
      //Get the JSON data of song details with embed code from SoundCloud oEmbed
      $getValues=file_get_contents('http://soundcloud.com/oembed?format=js&url='.$url.'&iframe=true');
      //Clean the Json to decode
      $decodeiFrame=substr($getValues, 1, -2);
      //json decode to convert it as an array
      $jsonObj = json_decode($decodeiFrame);

      //Change the height of the embed player if you want else uncomment below line
      // echo $jsonObj->html;
      //Print the embed player to the page
      return str_replace('height="400"', 'height="140"', $jsonObj->html);
    }

  }
  
  function spotifyEmbed($link) {
    /**
     * Returns embed code
     */
    $url = filter_var($link , FILTER_SANITIZE_URL);

    if (filter_var($url, FILTER_VALIDATE_URL) == true) {
      $values = parse_url( $url );
      $playlistID = $values['path'];
      ob_start();
      ?>
      <div class="iframe-container-spotify">
        <iframe src="https://open.spotify.com/embed<?php echo $playlistID; ?>" width="100%" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
      </div>
      <?php
      $embed = ob_get_contents();
      ob_end_clean();
      return $embed;
    }

  }
  
  
  function spotifyCheck($link) {
    /**
     * Returns follow me widget from URL or URI
     */
    $url = filter_var($link , FILTER_SANITIZE_URL);

    if (filter_var($url, FILTER_VALIDATE_URL) == true) {
      $values = parse_url( $url );
      $artistID = $values['path'];
      $url = 'https://open.spotify.com/follow/1/?uri=spotify:artist:' . $artistID .'&size=detail&theme=light';
      return $url;
    } else {
     
      $embed = 'https://open.spotify.com/follow/1/?uri=' . $url . '&size=detail&theme=light' ;
      return $embed;
      
    }

  }

  function appleMusicLink( $link ){
    /**
     * Returns embed URL code for apple music playlist 
     */

     $url = filter_var( $link, FILTER_SANITIZE_URL ); 

     if ( filter_var( $url, FILTER_VALIDATE_URL ) == true ){
      $values = parse_url( $url );
      $playlist = $values['path'];
      $url = 'https://embed.music.apple.com' . $playlist;

      ob_start();
      ?>
      <div class="iframe-container-apple">
        <iframe allow="autoplay *; encrypted-media *;" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="<?php echo $url; ?>"></iframe>
      </div>
      <?php
      $embed = ob_get_contents();
      ob_end_clean();
      return $embed;
    }
    
  }

?>
<div class="bb-profile-grid bb-grid">
  <div id="item-body" class="item-body">
    <div class="item-body-inner">
      <div class="bp-profile-wrapper">
        <div class="bp-profile-content">
          <div class="profile public">
          
            <header class="entry-header profile-loop-header profile-header flex align-items-center">
              <h1 class="entry-title bb-profile-title">Music Download & Streaming Services</h1>
            </header>
          
          </div>

          <div class="bp-widget details">
          <!-- Tabs -->
          <div class="tabs">
            <div class="controls">
            <?php if( !empty( $spotify ) or !empty( $spotifyPlaylist ) ): ?>
              <button class="active"><?php get_template_part( 'icons/icon', 'spotify' ) ?>Spotify</button>
            <?php endif; ?>
            <?php if( !empty( $apple ) ): ?>
              <button> <?php get_template_part( 'icons/icon', 'apple' ) ?> Apple Music</button>
            <?php endif; ?>
            <?php if( !empty( $soundcloud ) ): ?>
              <button> <?php get_template_part( 'icons/icon', 'soundcloud' ) ?> Soundcloud</button>
            <?php endif; ?>
            <?php if( !empty( $bandcamp ) ): ?>
              <button> <?php get_template_part( 'icons/icon', 'bandcamp' ) ?> Bandcamp</button>
            <?php endif; ?>
            </div>
            <div class="tabs-container">

              <?php if( !empty( $spotify ) or !empty( $spotifyPlaylist ) ):?>
                <div class="tab spotify"><!-- Spotify -->
                <div class="row">
              <div class="<?php if( !empty( $spotifyPlaylist ) ): ?> col-md-4 <?php endif;?>">
                  <?php if( !empty( $spotify ) ): ?>
                    <iframe src="<?php echo spotifyCheck( $spotify ) ?>" width="300" height="56" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowtransparency="true"></iframe>
                  <?php endif; ?>
                  </div>
                  <div class="<?php if( !empty( $spotify ) ):?> col-md-8 <?php else: ?> col-md-12 <?php endif;?>">
                    <?php if( !empty( $spotifyPlaylist ) )
                      echo spotifyEmbed( $spotifyPlaylist );?> 
                  </div>
                </div>
              </div><!-- /Spotify -->
              <?php endif; ?>

              <?php if( !empty( $apple ) ): ?>
                <div class="tab apple">
                    <?php if( !empty( $apple ) ) 
                    echo appleMusicLink( $apple ); ?> 
              </div>
              <?php endif;?>

              <?php if( !empty($soundcloud) ): ?>
                <div class="tab soundcloud">
                  <div class="iframe-container-soundcloud">
                    <?php echo soundcloudEmbed( $soundcloud ); ?>
                  </div>
                </div>
              <?php endif; ?>

              <?php if( !empty( $bandcamp ) ): ?>
                <div class="tab bandcamp">
                  <?php 
                  $graph = OpenGraph::fetch( $bandcamp );
                  $url = $graph->video;     
                  ?>
                  <div class="iframe-container-bandcamp">
                    <iframe style="border: 0; width: 100%; height: 120px;" src="<?php echo $url ?>" frameborder="0"></iframe>
                  </div>
                </div>
              <?php endif; ?>
            </div>
          </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>