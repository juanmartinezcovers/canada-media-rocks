<?php 
/**
 * Get User ID and edit profile link
 */
  $userID = bbp_get_user_id();
  $edit_profile_link = trailingslashit( bp_displayed_user_domain() . bp_get_profile_slug() . '/edit' );
/**
 * Fetching fields+
 */

  $repeaterField = BP_REST_XProfile_Repeater_Endpoint::get_repeater_fields( 86, $userID, 4 );
  

  function spotifyEmbed($link) {
    /**
     * Returns ID of spotify playlist, track or album
     */
    $url = filter_var($link , FILTER_SANITIZE_URL);

    if (filter_var($url, FILTER_VALIDATE_URL) == true) {
      $values = parse_url( $url );
      $playlistID = $values['path'];
      return $playlistID;
    }

  }


?>
<div class="bb-profile-grid bb-grid">
  <div id="item-body" class="item-body">
    <div class="item-body-inner">
      <div class="bp-profile-wrapper">
        <div class="bp-profile-content">
          <div class="profile public">
          
            <header class="entry-header profile-loop-header profile-header flex align-items-center">
              <h1 class="entry-title bb-profile-title">Music Library</h1>
              <?php if( bp_is_my_profile() ): ?>
                <a href="<?php echo $edit_profile_link; ?>" class="push-right button outline small"><?php esc_attr_e( 'Edit Profile', 'buddyboss-theme' ); ?></a>
	            <?php endif; ?>
            </header>
          
          </div>
          <div class="bp-widget details">
            <div class="row">
            <?php if( !empty( $repeaterField ) ): ?>
            <?php foreach( $repeaterField as $key => $value ):
                  $playlist = xprofile_get_field_data($key, $userID);
            ?>
                <div class="col-md-4">
                  <div class="embed-wrapper">
                  <?php  if ( $playlist != null ): ?>
                    <iframe src="https://open.spotify.com/embed<?php echo spotifyEmbed($playlist); ?>" width="100%" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                  <?php else: ?>
                    <?php if( bp_is_my_profile() ): ?>
                      <p>Ups, it seems that you don't have any playlists added. Edit your profile to show your spotify music</p>
                    <? else: ?>
                      <p>It seems that <?php echo bp_get_displayed_user_fullname(bp_displayed_user_id());  ?> doesn't have any playlist uploaded</p>
                    <?php endif; ?>
                  <?php endif;?>
                  </div>
                </div>
                <?php endforeach; ?>
            <?php else: ?>
              <?php if( bp_is_my_profile() ): ?>
                <p>Ups, it seems that you don't have any playlists added. Edit your profile to show your spotify music</p>
              <? else: ?>
                <p>It seems that <?php echo bp_get_displayed_user_fullname(bp_displayed_user_id());  ?> doesn't have any videos uploaded</p>
              <?php endif; ?>
            <?php endif;?>
            </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>
