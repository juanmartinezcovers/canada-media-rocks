<?php 
  /**
   * Get Group ID
   */
  global $bp, $wpdb;
  $groupID = $bp->groups->current_group->id;

  /**
   * Fetching fields+
   */
  $bandID = groups_get_groupmeta( $groupID, 'e2e_band-ec' );
?>
<div class="bb-profile-grid bb-grid">
  <div id="item-body" class="item-body">
    <div class="item-body-inner">
      <div class="bp-profile-wrapper">
        <div class="bp-profile-content">
          <div class="profile public">
          
            <header class="entry-header profile-loop-header profile-header flex align-items-center">
              <h1 class="entry-title bb-profile-title">Upcoming Shows</h1>
            </header>
          
          </div>

          <div class="bp-widget details">
            <?php 
              if ( empty( $bandID ) ) {
                get_template_part( 'tribe-shows', 'empty' );
              } else {

                global $post;
                
                //Arguments for the query
                $args = array(
                  'organizer'    => $organizer_id,
                  'eventDisplay' => 'upcoming'
                );

                $events = tribe_get_events( $args );

                foreach( $events as $post ){
                  setup_postdata( $post );
                  get_template_part( 'template-parts/tribe-shows', 'profile' );
                }
              }
            ?>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
