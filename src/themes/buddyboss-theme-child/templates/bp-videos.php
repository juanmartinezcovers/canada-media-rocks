<?php 
/**
 * Get User ID
 */
  $userID = bbp_get_user_id();

  //Editing link
  $edit_profile_link = trailingslashit( bp_displayed_user_domain() . bp_get_profile_slug() . '/edit' );

  $repeaterField = BP_REST_XProfile_Repeater_Endpoint::get_repeater_fields( 335, $userID, 14 );
  /**
   * Get Youtube ID from URL
   */
  function getYoutubeID($url){
    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
    $youtubeID = $match[1];
    return $youtubeID;
  }
?>

<div class="bb-profile-grid bb-grid">
  <div id="item-body" class="item-body">
    <div class="item-body-inner">
      <div class="bp-profile-wrapper">
        <div class="bp-profile-content">
          <div class="profile public">
          <header class="entry-header profile-loop-header profile-header flex align-items-center">
              <h1 class="entry-title bb-profile-title">Videos</h1>
              <?php if( bp_is_my_profile() ): ?>
                <a href="<?php echo $edit_profile_link; ?>" class="push-right button outline small"><?php esc_attr_e( 'Edit Profile', 'buddyboss-theme' ); ?></a>
	            <?php endif; ?>
            </header>
            <div class="bp-widget details">
                <div class="row">
                <?php if ( !empty( $repeaterField ) ): ?>

                <div class="youtube-videos-slider w-100">

                  <?php foreach( $repeaterField as $key => $value ):
                          $url = xprofile_get_field_data($key, $userID);?>
                   <iframe src="https://www.youtube.com/embed/<?php echo getYoutubeID( $url ); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                  <?php endforeach;?>

                </div>
                <?php else: ?>
                  <?php if( bp_is_my_profile() ): ?>
                    <p>Ups, it seems that you don't have any playlists added. Edit your profile to show your videos</p>
                  <? else: ?>
                    <p>It seems that <?php echo bp_get_displayed_user_fullname(bp_displayed_user_id());  ?> doesn't have any videos uploaded</p>
                  <?php endif; ?>
                <?php endif; ?>
                </div>
            </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>