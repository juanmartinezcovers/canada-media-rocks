<?php 
/**
 * Get User ID
 */
  $userID = bbp_get_user_id();
  
  $repeaterField = BP_REST_XProfile_Repeater_Endpoint::get_repeater_fields( 339, $userID, 15 );
?>

<div class="bb-profile-grid bb-grid">
  <div id="item-body" class="item-body">
    <div class="item-body-inner">
      <div class="bp-profile-wrapper">
        <div class="bp-profile-content">
          <div class="profile public">
            <header class="entry-header profile-loop-header profile-header flex align-items-center">
              <h1 class="entry-title bb-profile-title">Shows</h1>
            </header>
            <div class="bp-widget details">
              <?php 
              foreach( $repeaterField as $key => $value ){ 

                //Getting the ID's from the Band(s) 
                $artistID = xprofile_get_field_data($key, $userID); 
                $title = strip_tags( $artistID );
                $postObject = get_page_by_title( $title , OBJECT , 'tribe_organizer' );
                $organizer_id = $postObject->ID; 
                
                
                //Fetching events from tribe events
                if ( empty($organizer_id) ){
                  get_template_part( 'template-parts/tribe-shows', 'empty' );
                } else {

                  //Scoping global post variable
                  global $post;
                  
                  //Arguments for the query
                  $args = array(
                    'organizer'    => $organizer_id,
                    'eventDisplay' => 'upcoming'
                  );

                  $events = tribe_get_events( $args );

                  if ( empty( $events ) ) {
                    get_template_part( 'template-parts/tribe-shows', 'empty' );
                  } else {

                    foreach( $events as $post ){
                      setup_postdata( $post );
                      get_template_part( 'template-parts/tribe-shows', 'profile' );
                    }

                  }
                }                
              }
                ?>
            </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>