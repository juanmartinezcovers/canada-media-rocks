<?php 

  /**
   * Get Group ID
   */
  global $bp, $wpdb;
  $groupID = $bp->groups->current_group->id;

  /**
   * Fetching fields+
   */
  //Basic Information
  $hometown = groups_get_groupmeta( $groupID , 'e2e_hometown' );
  $currentLocation = groups_get_groupmeta( $groupID, 'e2e_current-location' );
  $typeOfMusic = groups_get_groupmeta( $groupID, 'e2e_type-of-music' );

  //Band Manager
  $managerCompany = groups_get_groupmeta( $groupID, 'e2e_manager-company' );
  $managerName = groups_get_groupmeta( $groupID, 'e2e_manager-name');
  $managerEmail = groups_get_groupmeta( $groupID, 'e2e_manager-mail' );

  //Booking Manger
  $bookingCompany = groups_get_groupmeta( $groupID, 'e2e_booking-company' );
  $bookingName = groups_get_groupmeta( $groupID, 'e2e_booking-name' );
  $bookingEmail = groups_get_groupmeta( $groupID, 'e2e_booking-mail' );

  //Record Label
  $recordCompany = groups_get_groupmeta( $groupID, 'e2e_record-company');
  $recordName = groups_get_groupmeta( $groupID, 'e2e_record-name' );
  $recordEmail = groups_get_groupmeta( $groupID, 'e2e_record-mail' );

  //Press Contact
  $pressCompany = groups_get_groupmeta( $groupID, 'e2e_press-company');
  $pressName = groups_get_groupmeta( $groupID, 'e2e_press-name' );
  $pressEmail = groups_get_groupmeta( $groupID, 'e2e_press-mail' );

  $genres = array(
    'Rock n Roll',
    'Indie Rock',
    'Classic Rock',
    'Alternative Rock',
    'Rock a Billy',
    'Pop Rock',
    'Glam Rock',
    'Surf Rock',
    'Garage Rock',
    'Root Rock',
    'Progressive Rock',
    'Punk Rock',
    'New Wave',
    'Post Punk',
    'Heavy Metal',
    'Hair Metal',
    'Scream Metal',
    'Goth',
    'Grunge',
    'Post Grunge',
    'Folk',
    'Folk Rock',
    'Blues',
    'Blues Rock',
    'Jazz',
    'Jazz Rock',
    'Swing Jazz'
  );

?>
<div class="bb-profile-grid bb-grid">
  <div id="item-body" class="item-body">
    <div class="item-body-inner">
      <div class="bp-profile-wrapper">
        <div class="bp-profile-content">
          <div class="profile public">
          
            <header class="entry-header profile-loop-header profile-header flex align-items-center">
              <h1 class="entry-title bb-profile-title">Band Profile Details</h1>
            </header>
          
          </div>

          <div class="bp-widget details">

            <div class="group-location">
              <h2>Location</h2>
              
              <div class="d-flex group-location-hometown">
                <div class="mr-2"> 
                  <p>Hometown:</p>
                </div>
                <div>
                  <p><?php echo $hometown?></p>
                </div>
              </div>

              <div class="d-flex group-location-current">
                <div class="mr-2">
                  <p>Current Location: </p>
                </div>
                <div>
                  <p><?php  echo $currentLocation ?></p>
                </div>
              </div>
            </div>

            <div class="group-fav-music">
              <div class="d-flex">
                <div class="mr-2">
                  <p>Favorite Music Genres: </p>
                </div>
                <div>
                  <p class="genres-text">
                  <?php 
                  foreach( $genres as $genre ) {
                    $handler = str_replace( ' ', '-', strtolower( $genre ) ); 

                    if ( !empty( groups_get_groupmeta( $groupID, 'e2e_' . $handler ) ) ) {
                      echo ' - ' . groups_get_groupmeta( $groupID, 'e2e_' . $handler );
                    }

                   $i++;
                  }         
                  ?>
                  </p>
                </div>
              </div>
            </div>

            <div class="group-band-manager">
              <h2>Band Manager</h2>

              <div class="d-flex group-band-manager-company">
                <div class="mr-2">
                  <p>Company Name: </p>
                </div>
                <div>
                  <p><?php echo $managerCompany; ?></p>
                </div>
              </div>

              <div class="d-flex group-band-manager-name">
                <div class="mr-2">
                  <p>Manager Name: </p>
                </div>
                <div>
                  <p><?php echo $managerName; ?></p>
                </div>
              </div>

              <div class="d-flex group-band-manager-mail">
                <div class="mr-2">
                  <p>Manager Email: </p>
                </div>
                <div>
                  <p><?php echo $managerEmail; ?></p>
                </div>
              </div>
            </div>
            <hr>

            <div class="group-band-booking">
              <h2>Booking Manager</h2>

              <div class="d-flex group-band-booking-company">
                <div class="mr-2">
                  <p>Company Name: </p>
                </div>
                <div>
                  <p><?php echo $bookingCompany; ?></p>
                </div>
              </div>

              <div class="d-flex group-band-booking-name">
                <div class="mr-2">
                  <p>Contact Name: </p>
                </div>
                <div>
                  <p><?php echo $bookingName; ?></p>
                </div>
              </div>

              <div class="d-flex group-band-booking-mail">
                <div class="mr-2">
                  <p>Contact Email: </p>
                </div>
                <div>
                  <p><?php echo $bookingEmail; ?></p>
                </div>
              </div>
            </div>
            <hr>

            <div class="group-band-record">
              <h2>Record Label</h2>

              <div class="d-flex group-band-record-company">
                <div class="mr-2">
                  <p>Record Label Name: </p>
                </div>
                <div>
                  <p><?php echo $recordCompany; ?></p>
                </div>
              </div>

              <div class="d-flex group-band-record-name">
                <div class="mr-2">
                  <p>Contact Name: </p>
                </div>
                <div>
                  <p><?php echo $recordName; ?></p>
                </div>
              </div>

              <div class="d-flex group-band-record-mail">
                <div class="mr-2">
                  <p>Contact Email: </p>
                </div>
                <div>
                  <p><?php echo $recordEmail; ?></p>
                </div>
              </div>
            </div>
            <hr>

            <div class="group-band-press">
              <h2>Press</h2>

              <div class="d-flex group-band-record-company">
                <div class="mr-2">
                  <p>Press Company Name: </p>
                </div>
                <div>
                  <p><?php echo $pressCompany; ?></p>
                </div>
              </div>

              <div class="d-flex group-band-press-name">
                <div class="mr-2">
                  <p>Press Contact Name: </p>
                </div>
                <div>
                  <p><?php echo $pressName; ?></p>
                </div>
              </div>

              <div class="d-flex group-band-press-mail">
                <div class="mr-2">
                  <p>Contact Email: </p>
                </div>
                <div>
                  <p><?php echo $pressEmail; ?></p>
                </div>
              </div>
            </div>

            
           
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
