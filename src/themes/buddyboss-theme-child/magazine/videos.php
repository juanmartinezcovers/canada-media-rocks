<section class="videos-magazine-section">
  <div class="container videos-magazine-container">
    <h3 class="title-video-releases">New Video Releases</h3>
    <?php 
    $terms = array(
      array(
        'taxonomy' => 'post_format',
        'field'    => 'slug',
        'terms'    => array('post-format-video'),
        'operator' => 'IN'
      )
    );
    
    $args = array(
      'posts_per_page'   => '8',
      'order'            => 'DESC',
      'orderby'          => 'post_date',
      'tax_query'        => $terms

    );

    $videos = new WP_Query( $args );
    ?>

    <?php if( $videos->have_posts() ): ?>
        <div class="videos-parent-container px-2 px-md-5">
          <?php while( $videos->have_posts() ) : 
            $videos->the_post();
            $img = get_the_post_thumbnail_url( get_the_id(), "full" );
          ?>
          <div class="videos-parent-image d-flex flex-column align-items-center justify-content-center" style="background-image: url('<?php echo $img; ?>')">
            <a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
          </div>
          <?php endwhile;?>
        </div>

        <div class="videos-child-container">
        <?php 
        while( $videos->have_posts() ):
          $videos->the_post();
          $img = get_the_post_thumbnail_url( get_the_id(), 'full' ); 
          ?>
          <div class="video-slide-nav">
            <img src="<?php echo $img; ?>" alt="<?php echo get_the_post_thumbnail_caption( get_the_id() );?>">
           <div class="play-button-magazine">
            <a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>">
            <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-play-fill" fill="white" xmlns="http://www.w3.org/2000/svg">
              <path d="M11.596 8.697l-6.363 3.692c-.54.313-1.233-.066-1.233-.697V4.308c0-.63.692-1.01 1.233-.696l6.363 3.692a.802.802 0 0 1 0 1.393z"/>
            </svg>
            </a>
           </div>
          </div>
        <?php endwhile; ?>
        </div>
    <?php endif;
    wp_reset_postdata();
    ?>
    
  </div>
</section>