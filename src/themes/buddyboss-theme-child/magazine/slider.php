<?php 

$shows = wp_get_attachment_image_url( 52940 , 'full', '' );
$venues = wp_get_attachment_image_url( 52941 , 'full', '' );
$bands = wp_get_attachment_image_url( 52942 , 'full', '' );
$login = wp_get_attachment_image_url( 52943 , 'full', '' );
$loginText = is_user_logged_in() ? 'Dashboard' : 'Sign In';
$login_url = is_user_logged_in() ? '/news-feed' : '/wp-login.php';

 $args = array(
  'posts_per_page' => '4',
  'order'          => 'DESC',
  'orderby'        => 'post_date',
  'meta_key'       => 'meta-checkbox',
  'meta_value'     => 'yes'

);

$featured = new WP_Query( $args );
?>
<section class="hero-posts">
    <div class="container-slider">
      <?php if( $featured->have_posts() ): ?>
    <div class="slider-hero">
    <?php while( $featured->have_posts() ):
            $featured->the_post();
            $featuredImg = get_the_post_thumbnail_url( get_the_id(), 'full' );?>
      <div class="slide">
        <div class="slide-information" style="background-image: url('<?php echo $featuredImg ?>');">
         <div class="container">
           <!-- Display Category -->
           <div class="category-slider">
           <?php $category = get_the_category(); ?>
            <a href="<?php echo get_category_link( $category[0]->term_id ); ?>"><?php echo $category[0]->cat_name ?></a>
           </div>
           <!-- Display the Title as a link to the Post's permalink. -->
          <h2 class="slide-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
          <small> <span class="author"><?php echo get_the_author(); ?></span> | <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
          <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
        </svg> <?php the_time('M j'); ?></small>
         </div>
        </div>
      </div>
      <?php endwhile; ?>
    </div>
    </div>
    <?php endif; ?>

    <!-- <div class="container container-slider-parent d-none d-md-block">
    <?php // if( $featured->have_posts() ): ?>
      <div class="row no-gutters nav-slider-parent-container">
        <?php // while( $featured->have_posts() ):
          // $featured->the_post();
          // $featuredImg = get_the_post_thumbnail_url( get_the_id(), 'full' ); ?>
        <div class="slider-nav-col col-md-3">
          <div class="overlay-slider-nav" style='background-image: url(<?php // echo $featuredImg ?>);'></div>

          <div class="inner-slider-nav d-flex flex-column">
            <div class="slider-nav-category category-slider">
            <?php // $category = get_the_category(); ?>
            <a href="<?php // echo get_category_link( $category[0]->term_id ); ?>"><?php // echo $category[0]->cat_name ?></a>
            </div>
            <h2 class="slide-title"><a href="<?php // the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php // the_title_attribute(); ?>"><?php // the_title(); ?></a></h2>
          </div>

        </div> 
        <?php // endwhile;?> 
      </div>
    </div>
    <?php // endif; 
     //wp_reset_postdata();
    ?> -->

    <div class="container container-magazine-parent d-none d-md-block">
      <div class="row no-gutters nav-magazine-parent-container">

        <!-- Tickets -->
        <div class="magazine-nav-col col-md-3">
          <div class="inner-magazine-nav d-flex flex-column justify-content-center align-items-center" style="background-image: url('<?php echo $shows; ?>');">
            <a href="/events" class="magazine-nav">Shows Tickets</a>
          </div>
        </div>

        <!-- Venues -->
        <div class="magazine-nav-col col-md-3">
          <div class="inner-magazine-nav d-flex flex-column justify-content-center align-items-center" style="background-image: url('<?php echo $venues; ?>');">
            <a href="/venues" class="magazine-nav">Venues</a>
          </div>
        </div>

        <!-- Venues -->
        <div class="magazine-nav-col col-md-3">
          <div class="inner-magazine-nav d-flex flex-column justify-content-center align-items-center" style="background-image: url('<?php echo $bands; ?>');">
            <a href="/bands" class="magazine-nav">Bands & Musicians</a>
          </div>
        </div>

        <!-- Venues -->
        <div class="magazine-nav-col col-md-3">
          <div class="inner-magazine-nav d-flex flex-column justify-content-center align-items-center" style="background-image: url('<?php echo $login; ?>');">
            <a href="<?php echo $login_url; ?>" class="magazine-nav"><?php echo $loginText; ?></a>
          </div>
        </div>

      </div>
    </div>
</section>