<section class="newsletters py-5">
  <div class="container">
    <h3>Subscribe to the news</h3>
    <form action="/" class="d-flex mt-3 justify-content-center">
      <div class="d-flex input-mail align-items-center">
      <?php get_template_part( 'icons/icon', 'envelope' ); ?>
        <input type="email" name="mail" id="mail">
      </div>
      <input type="submit" value="Sign Up" disabled>
    </form>
  </div>
</section>