<?php 
$args = array(
  'posts_per_page' => '8',
  'order' => 'DESC',
  'orderby' => 'post_date',
);

$tabPosts = new WP_Query( $args ); 
$maxPages = $tabPosts->max_num_pages;
?>

<section class="magazine-tabs mt-md-n4 mt-n2">
  <div class="container container-tabs">
    <div class="d-block d-md-none features-mobile">
      <h3>Features</h3>
    </div>
    <div class="tab-controls d-flex justify-content-between align-items-center">
      <div class="heading-tabs d-none d-md-block"><h3 class="mb-0">Features</h3></div>
      <div class="tabs-categories d-flex">
        <div class="category active-tab" data-tab-link="all">All</div>
        <?php 
          
          $categories = get_categories( array(
            'orderby' => 'name',
            'parent'  => 0,
            'hide_empty' => true,
            'exclude' => array(),
          ));

          $exclude = array("Uncategorized");

          foreach( $categories as $category ){
            if(!in_array($category->name, $exclude)):
            ?>
              <div class="category d-none d-md-block <?php echo strtolower($category->name); ?>" data-tab-link="<?php echo strtolower($category->name)?>"><?php echo $category->name ?></div>
            <?php endif;
          }
        ?>
      </div>
    </div>

    <div class="row tab-posts">
      <?php if( $tabPosts->have_posts() ): ?>
      <?php while( $tabPosts->have_posts() ):
          $tabPosts->the_post();
          $featuredImg = get_the_post_thumbnail_url( get_the_id(), "full" );
        ?>
        <div class="col-md-3 post-column">
          <div class="tab-post-container d-flex flex-column">
            <div class="tab-post-thumbnail">
              <a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><img src="<?php echo $featuredImg ?>" alt=""></a>
            </div>
            <div class="category-and-date mt-3">
            <?php $category = get_the_category(); ?>
            <a href="<?php echo get_category_link( $category[0]->term_id ); ?>"><?php echo $category[0]->cat_name ?></a>
            <?php the_time("M j"); ?>
            </div>
            <h3 class="tab-post-title mt-3"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
            <div class="tab-post-excerpt">
              <?php the_excerpt(); ?>
            </div>
          </div>
        </div>
      <?php endwhile; ?>
      <?php endif; wp_reset_postdata();?>
    </div>
    
    <div class="d-flex justify-content-center">
        <button class="load-more" data-page="1" data-max-page="<?php echo $maxPages ?>">Load More <?php get_template_part( 'magazine/includes/ajax', 'load-more-icon' )?> </button>
    </div>
  </div>
</section>