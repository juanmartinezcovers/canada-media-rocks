<?php  

$fashion = wp_get_attachment_image_url( 52948 , 'full' , '' );
$techAndGear = wp_get_attachment_image_url( 52938, 'full' , '' );

?> 

<section class="column-categories d-none d-md-block">
  <div class="row no-gutters">
   
    <!--col-6-->
    <div class="col-md-6" style="background-image: url('<?php echo $fashion; ?>'); background-position: center; background-size: cover;">
      <div class="d-flex flex-column justify-content-center align-items-center column-category">
        <a href="/category/fashion" class="category">Fashion</a>
      </div>
    </div><!--end-col-->
   
    <!--col-6-->
    <div class="col-md-6" style="background-image: url('<?php echo $techAndGear; ?>'); background-position: center; background-size: cover;">
      <div class="d-flex flex-column justify-content-center align-items-center column-category">
        <a href="/category/tech-and-gear" class="category">Tech & Gear</a>
      </div>
    </div><!--end-col-6-->

  </div>
</section>
