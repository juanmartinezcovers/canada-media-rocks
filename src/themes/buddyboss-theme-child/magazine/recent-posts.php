<?php 

$args = array(
  'posts_per_page'   => '4',
  'order'            => 'DESC',
  'orderby'          => 'post_date'
);

$recentPosts = new WP_Query( $args ); 
?>

<section class="recent-posts-section">
  <div class="container recent-posts-container">
   <div class="columns-recent-posts row">

   <div class="col-md-8">
   <h3 class="recent-posts-title">Lastest News</h3>
   <hr class="d-block d-md-none">
   <?php if( $recentPosts->have_posts() ): 
            while( $recentPosts->have_posts() ): 
              $recentPosts->the_post();
              $img = get_the_post_thumbnail_url( get_the_id(), "full" ); ?>
              <div class="row mb-5 align-items-center">
                <div class="col-md-5 featured-image-recent-post">
                  <img src="<?php echo $img?>" alt="<?php $alt = get_the_post_thumbnail_caption( get_the_id() ); echo $alt; ?>">
                </div>
                <div class="col-md-7 content-recent-post">
                  <?php $category = get_the_category(); ?>
                  <a href="<?php echo get_category_link( $category[0]->term_id ); ?>" class="mb-3"><?php echo $category[0]->cat_name ?></a>
                  <?php the_time("M j"); ?>
                  <h4 class="post-title mb-3"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
                  <div class="tab-post-excerpt">
                    <?php the_excerpt(); ?>
                  </div>
                </div>
              </div>
    <?php  endwhile;
        endif;
    wp_reset_postdata(); ?>
   </div>

   <div class="col-md-4 magazine-sidebar d-none d-md-block">
      <?php get_template_part( 'magazine/includes/most-viewed', 'posts' ); ?>
      <?php get_template_part( 'magazine/includes/instagram', 'widget' ); ?>
   </div>
    
   </div>
  </div>
</section>