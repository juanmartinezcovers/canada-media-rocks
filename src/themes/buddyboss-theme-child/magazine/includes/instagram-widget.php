<div class="instagram-widget mt-3">
  <h3 class="text-center">Follow Us</h3>
  <?php echo do_shortcode('[instagram-feed]'); ?>
</div>