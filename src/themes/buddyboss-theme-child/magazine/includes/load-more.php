<?

add_action('wp_ajax_nopriv_load_more_magazine', 'get_more_magazine');
add_action('wp_ajax_load_more_magazine', 'get_more_magazine');

function get_more_magazine() {
  $category = $_POST['category'];
  $page = $_POST['page'];
  $terms = array(
    array(
      'taxonomy' => 'category',
      'field'    => 'slug',
      'terms'    => $category
    )
  );
  $args = array(
    'posts_per_page' => '8',
    'order'          => 'DESC',
    'orderby'        => 'post_date',
    'paged'          => $page,
    'tax_query'      => $terms
  );

  $all = array(
    'posts_per_page' => '8',
    'order'          => 'DESC',
    'orderby'        => 'post_date',
    'paged'          => $page         
  );

 

 if ( $category != 'all' ){
  $tabPosts = new WP_Query( $args );
  ob_start();
  if ($tabPosts->have_posts()){
    while($tabPosts->have_posts()){
      $tabPosts->the_post();
      ?>
        <div class="col-md-3 post-column">
          <div class="tab-post-container d-flex flex-column">
            <div class="tab-post-thumbnail">
              <a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><img src="<?php $featuredImg = get_the_post_thumbnail_url( get_the_id(), "full" ); echo $featuredImg ?>" alt=""></a>
            </div>
            <div class="category-and-date mt-3">
            <a href="<?php echo '/category/' . $category; ?>"><?php echo ucfirst( $category ); ?></a>
            <?php the_time("M j"); ?>
            </div>
            <h3 class="tab-post-title mt-3"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
            <div class="tab-post-excerpt">
              <?php the_excerpt(); ?>
            </div>
          </div>
        </div>
      <?php
    }
  }
  wp_reset_postdata();
  $response = ob_get_contents();
  ob_get_clean();
  wp_send_json( $response );
 } else {
   $tabPosts = new WP_Query( $all );
  ob_start();
  if ($tabPosts->have_posts()){
    while($tabPosts->have_posts()){
      $tabPosts->the_post();
      ?>
        <div class="col-md-3 post-column">
          <div class="tab-post-container d-flex flex-column">
            <div class="tab-post-thumbnail">
              <a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><img src="<?php $featuredImg = get_the_post_thumbnail_url( get_the_id(), "full" ); echo $featuredImg ?>" alt=""></a>
            </div>
            <div class="category-and-date mt-3">
            <?php $category = get_the_category(); ?>
            <a href="<?php echo get_category_link( $category[0]->term_id ); ?>"><?php echo $category[0]->cat_name ?></a>
            <?php the_time("M j"); ?>
            </div>
            <h3 class="tab-post-title mt-3"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
            <div class="tab-post-excerpt">
              <?php the_excerpt(); ?>
            </div>
          </div>
        </div>
      <?php
    }
  }
  wp_reset_postdata();
  $response = ob_get_contents();
  ob_get_clean();
  wp_send_json( $response );
 }
}