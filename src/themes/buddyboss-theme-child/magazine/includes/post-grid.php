<div class="col-md-3 post-column">
  <div class="tab-post-container d-flex flex-column">
    <div class="tab-post-thumbnail">
      <a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><img src="<?php $featuredImg = get_the_post_thumbnail_url( get_the_id(), "full" ); echo $featuredImg ?>" alt=""></a>
    </div>
    <div class="category-and-date mt-3">
    <?php $category = get_the_category(); ?>
    <a href="<?php echo get_category_link( $category[0]->term_id ); ?>"><?php echo $category[0]->cat_name ?></a>
    <?php the_time("M j"); ?>
    </div>
    <h3 class="tab-post-title mt-3"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
    <div class="tab-post-excerpt">
      <?php the_excerpt(); ?>
    </div>
  </div>
</div>