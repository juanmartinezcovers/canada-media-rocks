<div class="trending-posts">
          <h4 class="trending-title">Trending</h4>
          <?php 
              $args = array(
                'posts_per_page' => '5',
                'meta_key'       => 'wpb_post_views_count',
                'orderby'        => 'meta_value_num',
                'order'          => 'DESC'
              );

              $popular = new WP_Query( $args );

              if( $popular->have_posts() ):
                while( $popular->have_posts() ):
                  $popular->the_post();
                  $count_key = 'wpb_post_views_count';
                  $count = get_post_meta(get_the_ID(), $count_key, true);
                  $img = get_the_post_thumbnail_url( get_the_id(), "full" );
                  $commentCount = get_comments_number( get_the_ID() );
            ?>
          <div class="row trending-posts-container align-items-center mb-3">
            <div class="col-md-6 trending-thumbnail">
              <img src="<?php echo $img?>" alt="<?php $alt = get_the_post_thumbnail_caption( get_the_id() ); echo $alt; ?>">
            </div>
            <div class="col-md-6 trending-post">
              <h4 class="post-title mb-1"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
              <div class="d-flex align-items-center post-meta">

                <div class="count-view d-flex align-items-center">
                  <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z"/>
                    <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                  </svg>
                  <span class="count-views ml-3"> <?php echo $count; ?> </span>
                </div>

                <div class="comment-count d-flex align-items-center ml-3">
                  <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chat-square-text" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h2.5a2 2 0 0 1 1.6.8L8 14.333 9.9 11.8a2 2 0 0 1 1.6-.8H14a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h2.5a1 1 0 0 1 .8.4l1.9 2.533a1 1 0 0 0 1.6 0l1.9-2.533a1 1 0 0 1 .8-.4H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                    <path fill-rule="evenodd" d="M3 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 6a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 6zm0 2.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z"/>
                  </svg>
                  <span class="comment-counts ml-3"> <?php echo $commentCount; ?> </span>
                </div>
              </div>
            </div>
          </div>
          <?php 
                endwhile;
              endif;
              wp_reset_postdata();
            ?>
        </div>